﻿using System.Collections.ObjectModel;

namespace EVESDEToSQL.Constants
{
    internal static class CommandConstant
    {
        internal const string Import = "import";
        internal const string Export = "export";
        internal const string Help = "help";
        internal const string Version = "version";

        internal static Collection<string> AllowedCommands
        {
            get { return new Collection<string> { Import, Export, Help, SwitchConstant.Help }; }
        }
    }
}
