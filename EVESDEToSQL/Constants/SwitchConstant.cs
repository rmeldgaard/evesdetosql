﻿namespace EVESDEToSQL.Constants
{
    internal static class SwitchConstant
    {
        public const string Help = "/?";
        public const string ExportTables = "/et:";
        public const string AceEngine = "/ace";
        public const string NoCsvHeader = "/nocsvheader";
    }
}
