﻿using System.Collections.ObjectModel;

namespace EVESDEToSQL.Constants
{
    internal static class ArgumentConstant
    {
        // Importer
        internal const string NoRestore = "-norestore";
        internal const string NoYaml = "-noyaml";
        internal const string NoSqlite = "-nosqlite";

        // Exporter
        internal const string Sql = "-sql";
        internal const string Sqlite = "-sqlite";
        internal const string MySql = "-mysql";
        internal const string PostgreSql = "-postgresql";
        internal const string Csv = "-csv";
        internal const string Access = "-access";

        internal static Collection<string> AllowedImportArguments
        {
            get { return new Collection<string> { NoRestore, NoYaml, NoSqlite }; }
        }

        internal static Collection<string> AllowedExportArguments
        {
            get { return new Collection<string> { Sql, Sqlite, MySql, PostgreSql, Csv, Access }; }
        }
    }
}
