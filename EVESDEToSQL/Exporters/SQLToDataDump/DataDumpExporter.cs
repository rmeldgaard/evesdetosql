﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL.Exporters.SQLToDataDump
{
    internal sealed class DataDumpExporter : IExporter
    {
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataDumpExporter"/> class.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <param name="backup">The backup.</param>
        /// <exception cref="System.ArgumentNullException">
        /// sqlConnectionProvider
        /// or
        /// backup
        /// </exception>
        public DataDumpExporter(DbConnectionProvider sqlConnectionProvider, Backup backup)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException("sqlConnectionProvider");
            if (backup == null)
                throw new ArgumentNullException("backup");

            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;
            Backup = backup;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets the backup.
        /// </summary>
        /// <value>
        /// The backup.
        /// </value>
        internal Backup Backup { get; private set; }

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        public List<string> TablesToExport { get; set; }

        /// <summary>
        /// Exports the data.
        /// </summary>
        public void ExportData()
        {
            if (m_isClosing)
                return;

            string filePath = Path.Combine(
                new DirectoryInfo(Util.DataDumpExportsDirectory.FullName + "\\SQLServer").CreateIfAbsent(),
                String.Format(CultureInfo.InvariantCulture, "DATADUMP{0:yyyyMMddHHmm}.{1}", DateTimeOffset.Now,
                    TablesToExport.Any() ? "sql" : "bak"));

            if (TablesToExport.Any())
                m_sqlConnectionProvider.OpenConnection();

            string actionText = String.Format(CultureInfo.InvariantCulture,
                "Exporting database '{0}' to SQL data dump {1}file... ",
                m_sqlConnectionProvider.Connection.Database,
                TablesToExport.Any() ? String.Empty : "backup ");

            if (TablesToExport.Any())
                Console.WriteLine(actionText);
            else
                Console.Write(actionText);

            try
            {
                if (TablesToExport.Any())
                    ExportTablesBackup(filePath);
                else
                    ExportFullBackup(filePath);
            }
            catch (Exception ex)
            {
                string failText = String.Format(CultureInfo.InvariantCulture,
                    "Exporting database '{0}' to SQL data dump backup: Failed\n{1}",
                    m_sqlConnectionProvider.Connection.Database,
                    ex.Message);

                Util.HandleExceptionWithReason(actionText, failText, ex.GetMostInnerExceptionMessage());
            }
        }

        /// <summary>
        /// Exports the tables backup.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void ExportTablesBackup(string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            Console.WriteLine();

            ScriptingOptions options = new ScriptingOptions
            {
                FileName = filePath,
                ScriptSchema = true,
                ScriptData = true,
                ScriptDrops = false,
                EnforceScriptingOptions = true,
                IncludeHeaders = true,
                Indexes = true,
                AppendToFile = true,
                ToFileOnly = true
            };

            foreach (string tableName in TablesToExport)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                Util.ResetCounters();

                string actionText = String.Format(CultureInfo.InvariantCulture,
                    "Exporting table '{0}'... ", tableName);

                Console.Write(actionText);

                Util.UpdatePercentDone(0);
                
                Table table = m_sqlConnectionProvider.GetDatabase().Tables[tableName];

                if (table == null)
                {
                    Util.ResetConsoleCursorPosition(actionText + Util.DisplayedText);
                    Console.WriteLine(@"Table '{0}' does not exists!", tableName);
                    continue;
                }

                table.EnumScript(options);

                Util.UpdatePercentDone(1);

                Util.DisplayEndTime(stopwatch);
            }

            Console.WriteLine();
        }

        /// <summary>
        /// Exports the full backup.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void ExportFullBackup(string filePath)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            Database database = m_sqlConnectionProvider.GetDatabase();

            RecoveryModel recoveryMod = database.DatabaseOptions.RecoveryModel;

            if (recoveryMod != RecoveryModel.Simple)
                database.RecoveryModel = RecoveryModel.Simple;

            Backup.Database = database.Name;
            Backup.PercentCompleteNotification = 1;
            Backup.PercentComplete += Backup_PercentComplete;
            Backup.BackupSetName = String.Format(CultureInfo.InvariantCulture, "{0}-Full Database Backup",
                database.Name);
            Backup.BackupSetDescription = String.Format(CultureInfo.InvariantCulture, "Full backup of {0} database",
                database.Name);
            Backup.Devices.AddDevice(filePath, DeviceType.File);
            Backup.LogTruncation = BackupTruncateLogType.Truncate;
            Backup.Incremental = false;
            Backup.Initialize = true;

            if (m_isClosing)
                return;

            Backup.SqlBackup(m_sqlConnectionProvider.Server);
            Backup.Devices.Clear();
            database.RecoveryModel = recoveryMod;

            Util.DisplayEndTime(stopwatch);

            Console.WriteLine();
        }

        /// <summary>
        /// Handles the PercentComplete event of the Backup control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="PercentCompleteEventArgs"/> instance containing the event data.</param>
        private static void Backup_PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            Util.UpdatePercentDone(100);
        }
    }
}
