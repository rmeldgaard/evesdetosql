﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL.Exporters.SQLToSQLite
{
    internal sealed class SqliteExporter : IExporter
    {
        private readonly SqliteConnectionProvider m_sqliteConnectionProvider;
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqliteExporter"/> class.
        /// </summary>
        /// <param name="sqliteConnectionProvider">The sqlite connection provider.</param>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">
        /// sqliteConnectionProvider
        /// or
        /// sqlConnectionProvider
        /// </exception>
        public SqliteExporter(DbConnectionProvider sqliteConnectionProvider, DbConnectionProvider sqlConnectionProvider)
        {
            if (sqliteConnectionProvider == null)
                throw new ArgumentNullException("sqliteConnectionProvider");

            if (sqlConnectionProvider == null)
                throw new ArgumentNullException("sqlConnectionProvider");

            m_sqliteConnectionProvider = (SqliteConnectionProvider)sqliteConnectionProvider;
            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        public List<string> TablesToExport { get; set; }

        /// <summary>
        /// Exports the data.
        /// </summary>
        public void ExportData()
        {
            if (m_isClosing)
                return;

            string actionText = String.Format(CultureInfo.InvariantCulture,
                "Exporting database '{0}' to SQLite database file...",
                m_sqlConnectionProvider.Connection.Database);

            Console.WriteLine(actionText);
            Console.WriteLine();

            try
            {
                Database database = m_sqlConnectionProvider.GetDatabase();

                Dictionary<string, Table> dbTables = TablesToExport.Any()
                    ? TablesToExport.ToDictionary(key => key, value => database.Tables[value])
                    : database.Tables.Cast<Table>().ToDictionary(key => key.Name, value => value);

                foreach (KeyValuePair<string, Table> table in dbTables)
                {
                    if (table.Value == null)
                    {
                        Util.ResetConsoleCursorPosition(actionText + Util.DisplayedText);
                        Console.WriteLine(@"Table '{0}' does not exists!", table.Key);
                        continue;
                    }

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    Util.ResetCounters();

                    actionText = String.Format(CultureInfo.InvariantCulture,
                        "Exporting table '{0}'... ", table.Key);

                    Console.Write(actionText);

                    m_sqliteConnectionProvider.DropAndCreateTable(table.Value.Name);

                    DataTable dataTable = m_sqlConnectionProvider.GetDataTable(table.Value);

                    m_sqliteConnectionProvider.ImportDataBulk(dataTable);

                    Util.DisplayEndTime(stopwatch);
                }

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string failText = String.Format(CultureInfo.InvariantCulture,
                    "Exporting database '{0}' to SQLite database file: Failed\n{1}",
                    m_sqlConnectionProvider.Connection.Database,
                    ex.Message);

                actionText += Util.DisplayedText;

                Util.HandleException(actionText, failText);
            }
        }
    }
}
