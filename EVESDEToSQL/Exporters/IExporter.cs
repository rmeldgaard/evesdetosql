﻿using System.Collections.Generic;

namespace EVESDEToSQL.Exporters
{
    internal interface IExporter
    {

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        List<string> TablesToExport { get; set; }

        /// <summary>
        /// Exports the data.
        /// </summary>
        void ExportData();
    }
}
