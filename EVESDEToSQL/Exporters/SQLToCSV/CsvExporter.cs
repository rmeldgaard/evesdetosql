﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL.Exporters.SQLToCSV
{
    internal sealed class CsvExporter : IExporter
    {
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        public CsvExporter(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException("sqlConnectionProvider");

            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        public List<string> TablesToExport { get; set; }

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        public bool PrintHeader { get; set; }

        /// <summary>
        /// Exports the data.
        /// </summary>
        public void ExportData()
        {
            if (m_isClosing)
                return;

            if (m_sqlConnectionProvider.Connection.State == ConnectionState.Closed)
                m_sqlConnectionProvider.OpenConnection();

            string actionText = String.Format(CultureInfo.InvariantCulture,
                "Exporting database '{0}' to CSV files... ",
                m_sqlConnectionProvider.Connection.Database);

            Console.WriteLine(actionText);
            Console.WriteLine();

            try
            {
                Database database = m_sqlConnectionProvider.GetDatabase();

                Dictionary<string, Table> dbTables = TablesToExport.Any()
                    ? TablesToExport.ToDictionary(key => key, value => database.Tables[value])
                    : database.Tables.Cast<Table>().ToDictionary(key => key.Name, value => value);

                foreach (KeyValuePair<string, Table> table in dbTables)
                {
                    if (table.Value == null)
                    {
                        Util.ResetConsoleCursorPosition(actionText + Util.DisplayedText);
                        Console.WriteLine(@"Table '{0}' does not exists!", table.Key);
                        continue;
                    }

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    Util.ResetCounters();

                    actionText = String.Format(CultureInfo.InvariantCulture,
                        "Exporting table '{0}'... ", table.Key);

                    Console.Write(actionText);

                    DataTable dataTable = m_sqlConnectionProvider.GetDataTable(table.Value);

                    ExportToCsv(dataTable, PrintHeader);

                    Util.DisplayEndTime(stopwatch);
                }

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string failText = String.Format(CultureInfo.InvariantCulture,
                    "Exporting database '{0}' to SQLite database file: Failed\n{1}",
                    m_sqlConnectionProvider.Connection.Database,
                    ex.Message);

                actionText += Util.DisplayedText;

                Util.HandleException(actionText, failText);
            }
        }

        /// <summary>
        /// Exports the data table to a CSV file.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        private static void ExportToCsv(DataTable dataTable, bool printHeder)
        {
            StringBuilder sb = new StringBuilder();

            double rowCount = dataTable.Rows.Count * 2;

            if (printHeder)
            {
                foreach (DataColumn column in dataTable.Columns.Cast<DataColumn>())
                {
                    sb.AppendFormat("{0};", column.ColumnName);
                }
                sb.AppendLine();
            }

            foreach (DataRow row in dataTable.Rows.Cast<DataRow>())
            {
                Util.UpdatePercentDone(rowCount);

                foreach (var csvValue in row.ItemArray)
                {
                    var needQuotes = csvValue is string &&
                                 (((string)csvValue).IndexOf(";", StringComparison.InvariantCulture) >= 0
                                  || ((string)csvValue).IndexOf("\"", StringComparison.InvariantCulture) >= 0
                                  || ((string)csvValue).IndexOf(System.Environment.NewLine, StringComparison.InvariantCulture) >= 0);
                    if (needQuotes)
                    {
                        var item = ((string)csvValue).Replace("\"", "\"\"");
                        sb.AppendFormat("\"{0}\";", item);
                    }
                    else {
                        sb.AppendFormat("{0};", csvValue);
                    }
                }
                sb.AppendLine();
            }

            string path = Path.Combine(new DirectoryInfo(Util.DataDumpExportsDirectory.FullName + "\\CSV").CreateIfAbsent(),
                String.Format("{0}.csv", dataTable.TableName));

            File.WriteAllText(path, sb.ToString());
        }
    }
}
