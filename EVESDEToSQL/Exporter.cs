﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Exporters;
using EVESDEToSQL.Exporters.SQLToAccess;
using EVESDEToSQL.Exporters.SQLToCSV;
using EVESDEToSQL.Exporters.SQLToDataDump;
using EVESDEToSQL.Exporters.SQLToMySQL;
using EVESDEToSQL.Exporters.SQLToPostgreSQL;
using EVESDEToSQL.Exporters.SQLToSQLite;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL
{
    internal static class Exporter
    {
        private static DbConnectionProvider s_sqlConnectionProvider;
        private static DbConnectionProvider s_sqliteConnectionProvider;
        private static DbConnectionProvider s_oleDbConnectionProvider;
        private static IExporter s_dataDumpExporter;

        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Exporter"/> class.
        /// </summary>
        static Exporter()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;

            Console.WriteLine();

            if (s_dataDumpExporter != null && ((DataDumpExporter)s_dataDumpExporter).Backup != null)
                ((DataDumpExporter)s_dataDumpExporter).Backup.Abort();

            if (s_sqliteConnectionProvider != null)
                s_sqliteConnectionProvider.CloseConnection();

            if (s_oleDbConnectionProvider != null)
                s_oleDbConnectionProvider.CloseConnection();

            if (s_sqlConnectionProvider != null)
                s_sqlConnectionProvider.CloseConnection();
        }

        /// <summary>
        /// Starts the exporter with the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public static void Start(string[] args)
        {
            var tablesToExport = new List<string>();
            if (args.Any(x => x == SwitchConstant.ExportTables))
            {
                tablesToExport = args.SkipWhile(x => x != SwitchConstant.ExportTables)
                    .Skip(1)
                    .TakeWhile(x => x != CommandConstant.Import
                                    && !x.StartsWith("/", StringComparison.Ordinal)
                                    && !x.StartsWith("-", StringComparison.Ordinal))
                    .OrderBy(x => x)
                    .ToList();

                args = args.Except(new[] { SwitchConstant.ExportTables }).Except(tablesToExport).ToArray();
            }

            bool useAceEngine = false;
            if (args.Any(x => x == SwitchConstant.AceEngine))
            {
                useAceEngine = true;
                args = args.Except(new[] { SwitchConstant.AceEngine }).ToArray();
            }

            bool printHeader = true;
            if (args.Any(x => x == SwitchConstant.NoCsvHeader))
            {
                printHeader = false;
                args = args.Except(new[] { SwitchConstant.NoCsvHeader }).ToArray();
            }

            IEnumerable<string> notAllowedArguments = args
                .SkipWhile(x => x != CommandConstant.Export)
                .TakeWhile(x => x != CommandConstant.Import)
                .Except(CommandConstant.AllowedCommands)
                .Except(ArgumentConstant.AllowedExportArguments);

            if (notAllowedArguments.Any())
            {
                Util.ShowHelp();
                Console.ReadKey(true);
                return;
            }

            args = args.Intersect(ArgumentConstant.AllowedExportArguments).ToArray();

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            try
            {
                s_sqlConnectionProvider = new SqlConnectionProvider("name=EveStaticData");

                if (!args.Any() || args.Intersect(new[] { ArgumentConstant.Sql }).Any())
                    ExportSQLDataDump(tablesToExport);

                if (!args.Any() || args.Intersect(new[] { ArgumentConstant.Sqlite }).Any())
                    ExportSQLiteDump(tablesToExport);

                if (!args.Any() || args.Intersect(new[] { ArgumentConstant.MySql }).Any())
                    ExportMySQLDump(tablesToExport);

                if (!args.Any() || args.Intersect(new[] { ArgumentConstant.PostgreSql }).Any())
                    ExportPostgreSQLDump(tablesToExport);

                if (!args.Any() || args.Intersect(new[] { ArgumentConstant.Access }).Any())
                    ExportAccessDump(tablesToExport, useAceEngine);

                if (!args.Any() || args.Intersect(new[] { ArgumentConstant.Csv }).Any())
                    ExportCSVDump(tablesToExport, printHeader);

                if (s_sqlConnectionProvider.Connection != null &&
                    s_sqlConnectionProvider.Connection.State == ConnectionState.Open)
                {
                    s_sqlConnectionProvider.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                Trace.Write(ex.GetRecursiveStackTrace());

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine(@"An unhandled exception was thrown.");
                Console.WriteLine(@"For more info refer to the 'trace.txt' file.");
                Util.PressAnyKey(-1);
            }

            if (s_isClosing)
                return;

            Console.WriteLine();
            Console.WriteLine(@"Data dump files have been exported at:");
            Console.WriteLine(Util.DataDumpExportsDirectory);

            Console.WriteLine();
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture, "Exporting files completed in {0:g}",
                stopwatch.Elapsed));

            Util.PressAnyKey();
        }

        /// <summary>
        /// Exports the SQL data dump.
        /// </summary>
        /// <param name="tablesToExport">The tables to export.</param>
        private static void ExportSQLDataDump(List<string> tablesToExport)
        {
            if (s_isClosing)
                return;

            s_dataDumpExporter = new DataDumpExporter(s_sqlConnectionProvider, new Backup())
            {
                TablesToExport = tablesToExport
            };

            s_dataDumpExporter.ExportData();
        }

        /// <summary>
        /// Exports the sqlite dump.
        /// </summary>
        /// <param name="tablesToExport">The tables to export.</param>
        private static void ExportSQLiteDump(List<string> tablesToExport)
        {
            if (s_isClosing)
                return;

            if (s_sqlConnectionProvider.Connection.State == ConnectionState.Closed)
                s_sqlConnectionProvider.OpenConnection();

            string filePath = Path.Combine(
                new DirectoryInfo(Util.DataDumpExportsDirectory.FullName + "\\Sqlite").CreateIfAbsent(),
                String.Format(CultureInfo.InvariantCulture, "{0}{1:yyyyMMddHHmm}.db",
                    s_sqlConnectionProvider.Connection.Database, DateTimeOffset.Now));

            if (File.Exists(filePath))
                File.Delete(filePath);

            SQLiteConnection.CreateFile(filePath);

            string connectionString = String.Format(CultureInfo.InvariantCulture, "data source={0}", filePath);

            s_sqliteConnectionProvider = new SqliteConnectionProvider(connectionString);
            s_sqliteConnectionProvider.OpenConnection();

            IExporter sqliteExporter = new SqliteExporter(s_sqliteConnectionProvider, s_sqlConnectionProvider)
            {
                TablesToExport = tablesToExport
            };

            sqliteExporter.ExportData();

            s_sqliteConnectionProvider.CloseConnection();
        }

        /// <summary>
        /// Exports the MySQL dump.
        /// </summary>
        /// <param name="tablesToExport">The tables to export.</param>
        private static void ExportMySQLDump(List<string> tablesToExport)
        {
            if (s_isClosing)
                return;

            IExporter mySqlExporter = new MySqlExporter(s_sqlConnectionProvider)
            {
                TablesToExport = tablesToExport
            };

            mySqlExporter.ExportData();
        }

        /// <summary>
        /// Exports the PostgreSQL dump.
        /// </summary>
        /// <param name="tablesToExport">The tables to export.</param>
        private static void ExportPostgreSQLDump(List<string> tablesToExport)
        {
            if (s_isClosing)
                return;

            IExporter postgreSqlExporter = new PostgreSqlExporter(s_sqlConnectionProvider)
            {
                TablesToExport = tablesToExport
            };

            postgreSqlExporter.ExportData();
        }

        /// <summary>
        /// Exports the Access dump.
        /// </summary>
        /// <param name="tablesToExport">The tables to export.</param>
        /// <param name="useAceEngine">if set to <c>true</c> database will use the ACE engine.</param>
        private static void ExportAccessDump(List<string> tablesToExport, bool useAceEngine)
        {
            if (s_isClosing)
                return;

            if (s_sqlConnectionProvider.Connection.State == ConnectionState.Closed)
                s_sqlConnectionProvider.OpenConnection();

            string filePath = Path.Combine(
                new DirectoryInfo(Util.DataDumpExportsDirectory.FullName + "\\MSAccess").CreateIfAbsent(),
                String.Format(CultureInfo.InvariantCulture, "{0}{1:yyyyMMddHHmm}{2}",
                    s_sqlConnectionProvider.Connection.Database, DateTimeOffset.Now,
                    useAceEngine ? ".accdb" : ".mdb"));

            if (File.Exists(filePath))
                File.Delete(filePath);

            string connectionString = String.Format("Provider=Microsoft.{0}.OLEDB.{1}.0;Data Source={2}",
                useAceEngine ? "ACE" : "Jet", useAceEngine ? "12" : "4", filePath);

            try
            {
                AccessExporter.CreateFile(connectionString);
            }
            catch (COMException)
            {
                if (s_sqlConnectionProvider.Connection.State == ConnectionState.Open)
                    s_sqlConnectionProvider.CloseConnection();

                string failText = "Access Database Engine (x86) not registered on local machine.\n" +
                                  "More info: https://www.microsoft.com/en-us/download/details.aspx?id=13255";

                Util.HandleException(String.Empty, failText);
            }

            s_oleDbConnectionProvider = new OleDbConnectionProvider(connectionString);
            s_oleDbConnectionProvider.OpenConnection();

            IExporter accessExporter = new AccessExporter(s_oleDbConnectionProvider, s_sqlConnectionProvider)
            {
                TablesToExport = tablesToExport
            };

            accessExporter.ExportData();

            s_oleDbConnectionProvider.CloseConnection();
        }

        /// <summary>
        /// Exports the CSV dump.
        /// </summary>
        /// <param name="tablesToExport">The tables to export.</param>
        private static void ExportCSVDump(List<string> tablesToExport, bool printHeader)
        {
            if (s_isClosing)
                return;

            IExporter csvExporter = new CsvExporter(s_sqlConnectionProvider)
            {
                TablesToExport = tablesToExport,
                PrintHeader = printHeader
            };

            csvExporter.ExportData();
        }
    }
}
