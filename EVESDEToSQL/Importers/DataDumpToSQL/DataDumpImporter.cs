﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL.Importers.DataDumpToSQL
{
    internal class DataDumpImporter : IImporter
    {
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataDumpImporter"/> class.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <param name="restore">The restore.</param>
        /// <exception cref="System.ArgumentNullException">
        /// sqlConnectionProvider
        /// or
        /// restore
        /// </exception>
        internal DataDumpImporter(DbConnectionProvider sqlConnectionProvider, Restore restore)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException("sqlConnectionProvider");
            if (restore == null)
                throw new ArgumentNullException("restore");

            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;
            Restore = restore;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets the restore.
        /// </summary>
        /// <value>
        /// The restore.
        /// </value>
        internal Restore Restore { get; private set; }

        /// <summary>
        /// Imports the data.
        /// </summary>
        public void ImportData()
        {
            if (m_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string filePath = Util.CheckDataDumpExists().Single();
            
            string actionText = String.Format(CultureInfo.InvariantCulture, "Importing SQL data dump to '{0}' database... ",
                m_sqlConnectionProvider.Connection.Database);

            Console.Write(actionText);

            try
            {
                RestoreData(filePath);
            }
            catch (Exception ex)
            {
                string failText = String.Format(CultureInfo.InvariantCulture,
                    "Importing SQL data dump to '{0}' database: Failed\n{1}",
                    m_sqlConnectionProvider.Connection.Database,
                    ex.Message);

                Util.HandleExceptionWithReason(actionText, failText, ex.GetMostInnerExceptionMessage());
            }

            Util.DisplayEndTime(stopwatch);

            Console.WriteLine();
        }

        /// <summary>
        /// Restores the data.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void RestoreData(string filePath)
        {
            Server server = m_sqlConnectionProvider.Server;

            string defaultDataPath = String.IsNullOrEmpty(server.Settings.DefaultFile)
                ? server.MasterDBPath
                : server.Settings.DefaultFile;
            string defaultLogPath = String.IsNullOrEmpty(server.Settings.DefaultLog)
                ? server.MasterDBLogPath
                : server.Settings.DefaultLog;

            Restore.Database = m_sqlConnectionProvider.Connection.Database;
            Restore.ReplaceDatabase = true;
            Restore.PercentCompleteNotification = 1;
            Restore.PercentComplete += Restore_PercentComplete;
            Restore.Devices.AddDevice(filePath, DeviceType.File);
            Restore.RelocateFiles.AddRange(
                new[]
                {
                    new RelocateFile("ebs_DATADUMP",
                        String.Format(CultureInfo.InvariantCulture, "{0}{1}.mdf", defaultDataPath, Restore.Database)),
                    new RelocateFile("ebs_DATADUMP_log",
                        String.Format(CultureInfo.InvariantCulture, "{0}{1}_log.ldf", defaultLogPath, Restore.Database))
                });

            if (m_isClosing)
                return;

            Restore.SqlRestore(server);
            Restore.Devices.Clear();
        }

        /// <summary>
        /// Handles the PercentComplete event of the Restore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="PercentCompleteEventArgs"/> instance containing the event data.</param>
        private static void Restore_PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            Util.UpdatePercentDone(100);
        }
    }
}
