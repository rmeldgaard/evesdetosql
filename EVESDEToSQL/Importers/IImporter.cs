﻿namespace EVESDEToSQL.Importers
{
    internal interface IImporter
    {
        /// <summary>
        /// Imports the data.
        /// </summary>
        void ImportData();
    }
}