﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL
{
    internal static class SkinMaterials
    {
        private const string SknMaterialsTableName = "sknMaterials";

        private const string SkinMaterialIDText = "skinMaterialID";
        private const string MaterialSetIDText = "materialSetID";
        private const string DisplayNameIDText = "displayNameID";

        // Obsolete since Galatea 1.0
        private const string MaterialText = "material";
        private const string ColorHullText = "colorHull";
        private const string ColorWindowText = "colorWindow";
        private const string ColorPrimaryText = "colorPrimary";
        private const string ColorSecondaryText = "colorSecondary";

        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Util"/> class.
        /// </summary>
        static SkinMaterials()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the skin materials.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        internal static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException("sqlConnectionProvider");

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string yamlFile = YamlFilesConstants.SkinMaterials;
            string filePath = Util.CheckYamlFileExists(yamlFile);

            if (String.IsNullOrEmpty(filePath))
                return;

            string text = String.Format(CultureInfo.InvariantCulture, "Parsing {0}... ", yamlFile);
            Console.Write(text);
            YamlMappingNode rNode = Util.ParseYamlFile(filePath);

            if (s_isClosing)
                return;

            Util.ResetConsoleCursorPosition(text);

            if (rNode == null)
            {
                Console.WriteLine(@"Unable to parse {0}.", yamlFile);
                return;
            }

            Console.Write(@"Importing {0}... ", yamlFile);

            s_sqlConnectionProvider.DropAndCreateTable(SknMaterialsTableName);

            ImportDataBulk(rNode);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportDataBulk(YamlMappingNode rNode)
        {
            if (s_isClosing)
                return;

            Util.UpdatePercentDone(0);

            DataTable sknMaterialsTable = GetSknMaterialsDataTable();

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
            {
                Util.UpdatePercentDone(rowCount);

                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                DataRow row = sknMaterialsTable.NewRow();
                sknMaterialsTable.Rows.Add(row);

                row[SkinMaterialIDText] = SqlInt32.Parse(pair.Key.ToString());
                row[MaterialSetIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(MaterialSetIDText, defaultValue: "0");
                row[DisplayNameIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(DisplayNameIDText, defaultValue: "0");

                // Obsolete since Galatea 1.0
                row[MaterialText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(MaterialText, defaultValue: "");
                row[ColorHullText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(ColorHullText);
                row[ColorWindowText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(ColorWindowText);
                row[ColorPrimaryText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(ColorPrimaryText);
                row[ColorSecondaryText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(ColorSecondaryText);
            }

            s_sqlConnectionProvider.ImportDataBulk(sknMaterialsTable);

            Util.UpdatePercentDone(rowCount);
        }

        /// <summary>
        /// Gets the data table for the sknMaterials table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetSknMaterialsDataTable()
        {
            using (DataTable sknMaterialsTable = new DataTable(SknMaterialsTableName))
            {
                sknMaterialsTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(SkinMaterialIDText, typeof(SqlInt32)),
                        new DataColumn(MaterialSetIDText, typeof(SqlInt32)),
                        new DataColumn(DisplayNameIDText, typeof(SqlInt32)),

                        // Obsolete since Galatea 1.0
                        new DataColumn(MaterialText, typeof(SqlString)),
                        new DataColumn(ColorHullText, typeof(SqlString)),
                        new DataColumn(ColorWindowText, typeof(SqlString)),
                        new DataColumn(ColorPrimaryText, typeof(SqlString)),
                        new DataColumn(ColorSecondaryText, typeof(SqlString)),
                    });
                return sknMaterialsTable;
            }
        }
    }
}
