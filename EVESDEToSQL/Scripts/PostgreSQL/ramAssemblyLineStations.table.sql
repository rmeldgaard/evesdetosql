﻿-- ----------------------------
-- Table structure for table "ramAssemblyLineTypeDetailPerCategory"
-- ----------------------------

DROP TABLE IF EXISTS "public"."ramAssemblyLineStations";

CREATE TABLE "public"."ramAssemblyLineStations"(
	"stationID" int NOT NULL,
	"assemblyLineTypeID" smallint NOT NULL,
	quantity smallint NULL,
	"stationTypeID" int NULL,
	"ownerID" int NULL,
	"solarSystemID" int NULL,
	"regionID" int NULL,
	PRIMARY KEY ("stationID", "assemblyLineTypeID")
);

CREATE INDEX "ramAssemblyLineStations_IX_owner" ON "public"."ramAssemblyLineStations" ("ownerID" ASC);

CREATE INDEX "ramAssemblyLineStations_IX_region" ON "public"."ramAssemblyLineStations" ("regionID" ASC);
