﻿-- ----------------------------
-- Table structure for table "sknSkins"
-- ----------------------------

DROP TABLE IF EXISTS "public"."sknSkins";

CREATE TABLE "public"."sknSkins"(
	"skinID" int NOT NULL,
	"internalName" varchar(100) NOT NULL DEFAULT (''),
	"skinMaterialID" int NULL,
	"typeID" int NULL,
	"allowCCPDevs" bool NOT NULL DEFAULT (0)::bool,
	"visibleSerenity" bool NOT NULL DEFAULT (0)::bool,
	"visibleTranquility" bool NOT NULL DEFAULT (0)::bool,
	PRIMARY KEY ("skinID")
);

CREATE INDEX "sknSkins_IX_skinMaterial" ON "public"."sknSkins" ("skinMaterialID" ASC);
