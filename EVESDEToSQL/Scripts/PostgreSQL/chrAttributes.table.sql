﻿-- ----------------------------
-- Table structure for table "chrAttributes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."chrAttributes";

CREATE TABLE "public"."chrAttributes"(
	"attributeID" smallint NOT NULL,
	"attributeName" varchar(100) NULL,
	description varchar(1000) NULL,
	"iconID" int NULL,
	"shortDescription" varchar(500) NULL,
	notes varchar(500) NULL,
	PRIMARY KEY ("attributeID")
);