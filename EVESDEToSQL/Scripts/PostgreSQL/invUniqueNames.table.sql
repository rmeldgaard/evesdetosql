﻿-- ----------------------------
-- Table structure for table "invUniqueNames"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invUniqueNames";

CREATE TABLE "public"."invUniqueNames"(
	"itemID" int NOT NULL,
	"itemName" varchar(200) NOT NULL,
	"groupID" int NULL,
	PRIMARY KEY ("itemID")
);

CREATE INDEX "invUniqueNames_IX_GroupName" ON "public"."invUniqueNames" ("groupID" ASC, "itemName" ASC);

CREATE INDEX "invUniqueNames_UQ" ON "public"."invUniqueNames" ("itemName" ASC);
