﻿-- ----------------------------
-- Table structure for table "dgmAttributeTypes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmAttributeTypes";

CREATE TABLE "public"."dgmAttributeTypes"(
	"attributeID" smallint NOT NULL,
	"attributeName" varchar(100) NULL,
	description varchar(1000) NULL,
	"iconID" int NULL,
	"defaultValue" double precision NULL,
	published bool NULL,
	"displayName" varchar(100) NULL,
	"unitID" smallint NULL,
	stackable bool NULL,
	"highIsGood" bool NULL,
	"categoryID" smallint NULL,
	PRIMARY KEY ("attributeID")
);