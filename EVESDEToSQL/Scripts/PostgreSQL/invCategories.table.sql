﻿-- ----------------------------
-- Table structure for table "invCategories"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invCategories";

CREATE TABLE "public"."invCategories"(
	"categoryID" int NOT NULL,
	"categoryName" varchar(100) NULL,
	description varchar(3000) NULL,
	"iconID" int NULL,
	published bool NULL,
	PRIMARY KEY ("categoryID")
);