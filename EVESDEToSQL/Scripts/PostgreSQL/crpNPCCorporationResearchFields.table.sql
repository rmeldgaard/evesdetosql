﻿-- ----------------------------
-- Table structure for table "crpNPCCorporationResearchFields"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crpNPCCorporationResearchFields";

CREATE TABLE "public"."crpNPCCorporationResearchFields"(
	"skillID" int NOT NULL,
	"corporationID" int NOT NULL,
	PRIMARY KEY ("skillID", "corporationID")
);