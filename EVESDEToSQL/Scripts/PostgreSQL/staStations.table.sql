﻿-- ----------------------------
-- Table structure for table "staStations"
-- ----------------------------

DROP TABLE IF EXISTS "public"."staStations";

CREATE TABLE "public"."staStations"(
	"stationID" int NOT NULL,
	security smallint NULL,
	"dockingCostPerVolume" double precision NULL,
	"maxShipVolumeDockable" double precision NULL,
	"officeRentalCost" int NULL,
	"operationID" smallint NULL,
	"stationTypeID" int NULL,
	"corporationID" int NULL,
	"solarSystemID" int NULL,
	"constellationID" int NULL,
	"regionID" int NULL,
	"stationName" varchar(100) NULL,
	x double precision NULL,
	y double precision NULL,
	z double precision NULL,
	"reprocessingEfficiency" double precision NULL,
	"reprocessingStationsTake" double precision NULL,
	"reprocessingHangarFlag" smallint NULL,
	PRIMARY KEY ("stationID")
);

CREATE INDEX "staStations_IX_constellation" ON "public"."staStations" ("corporationID" ASC);

CREATE INDEX "staStations_IX_corporation" ON "public"."staStations" ("corporationID" ASC);

CREATE INDEX "staStations_IX_operation" ON "public"."staStations" ("operationID" ASC);

CREATE INDEX "staStations_IX_region" ON "public"."staStations" ("regionID" ASC);

CREATE INDEX "staStations_IX_system" ON "public"."staStations" ("solarSystemID" ASC);

CREATE INDEX "staStations_IX_type" ON "public"."staStations" ("stationTypeID" ASC);
