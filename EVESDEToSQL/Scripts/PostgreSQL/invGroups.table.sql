﻿-- ----------------------------
-- Table structure for table "invGroups"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invGroups";

CREATE TABLE "public"."invGroups"(
	"groupID" int NOT NULL,
	"categoryID" int NULL,
	"groupName" varchar(100) NULL,
	description varchar(3000) NULL,
	"iconID" int NULL,
	"useBasePrice" bool NULL,
	"allowManufacture" bool NULL,
	"allowRecycler" bool NULL,
	anchored bool NULL,
	anchorable bool NULL,
	"fittableNonSingleton" bool NULL,
	published bool NULL,
	PRIMARY KEY ("groupID")
);

CREATE INDEX "invGroups_IX_category" ON "public"."invGroups" ("categoryID" ASC);
