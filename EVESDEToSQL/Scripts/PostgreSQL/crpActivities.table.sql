﻿-- ----------------------------
-- Table structure for table "crpActivities"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crpActivities";

CREATE TABLE "public"."crpActivities"(
	"activityID" smallint NOT NULL,
	"activityName" varchar(100) NULL,
	description varchar(1000) NULL,
	PRIMARY KEY ("activityID")
);