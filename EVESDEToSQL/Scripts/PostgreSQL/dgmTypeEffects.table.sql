﻿-- ----------------------------
-- Table structure for table "dgmTypeEffects"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmTypeEffects";

CREATE TABLE "public"."dgmTypeEffects"(
	"typeID" int NOT NULL,
	"effectID" smallint NOT NULL,
	"isDefault" bool NULL,
	PRIMARY KEY ("typeID", "effectID")
);