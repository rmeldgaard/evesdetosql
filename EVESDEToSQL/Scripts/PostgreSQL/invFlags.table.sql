﻿-- ----------------------------
-- Table structure for table "invFlags"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invFlags";

CREATE TABLE "public"."invFlags"(
	"flagID" smallint NOT NULL,
	"flagName" varchar(200) NULL,
	"flagText" varchar(100) NULL,
	"orderID" int NULL,
	PRIMARY KEY ("flagID")
);