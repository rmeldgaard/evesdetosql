﻿-- ----------------------------
-- Table structure for table "dgmTypeAttributes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmTypeAttributes";

CREATE TABLE "public"."dgmTypeAttributes"(
	"typeID" int NOT NULL,
	"attributeID" smallint NOT NULL,
	"valueInt" int NULL,
	"valueFloat" double precision NULL,
	PRIMARY KEY ("typeID", "attributeID")
);