﻿-- ----------------------------
-- Table structure for table "mapConstellationJumps"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapConstellationJumps";

CREATE TABLE "public"."mapConstellationJumps"(
	"fromRegionID" int NULL,
	"fromConstellationID" int NOT NULL,
	"toConstellationID" int NOT NULL,
	"toRegionID" int NULL,
	PRIMARY KEY ("fromConstellationID", "toConstellationID")
);

CREATE INDEX "mapConstellationJumps_IX_fromRegion" ON "public"."mapConstellationJumps" ("fromRegionID" ASC);
