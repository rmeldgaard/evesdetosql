﻿-- ----------------------------
-- Table structure for table "invPositions"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invPositions";

CREATE TABLE "public"."invPositions"(
	"itemID" bigint NOT NULL,
	x double precision NOT NULL DEFAULT (0.0),
	y double precision NOT NULL DEFAULT (0.0),
	z double precision NOT NULL DEFAULT (0.0),
	yaw real NULL,
	pitch real NULL,
	roll real NULL,
	PRIMARY KEY ("itemID")
);