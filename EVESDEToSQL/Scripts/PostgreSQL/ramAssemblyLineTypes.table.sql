﻿-- ----------------------------
-- Table structure for table "ramAssemblyLineTypes
-- ----------------------------

DROP TABLE IF EXISTS "public"."ramAssemblyLineTypes";

CREATE TABLE "public"."ramAssemblyLineTypes"(
	"assemblyLineTypeID" smallint NOT NULL,
	"assemblyLineTypeName" varchar(100) NULL,
	description varchar(1000) NULL,
	"baseTimeMultiplier" double precision NULL,
	"baseMaterialMultiplier" double precision NULL,
	"baseCostMultiplier" double precision NULL,
	volume double precision NULL,
	"activityID" smallint NULL,
	"minCostPerHour" double precision NULL,
	PRIMARY KEY ("assemblyLineTypeID")
);