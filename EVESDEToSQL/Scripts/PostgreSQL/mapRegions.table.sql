﻿-- ----------------------------
-- Table structure for table "mapRegions"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapRegions";

CREATE TABLE "public"."mapRegions"(
	"regionID" int NOT NULL,
	"regionName" varchar(100) NULL,
	x double precision NULL,
	y double precision NULL,
	z double precision NULL,
	"xMin" double precision NULL,
	"xMax" double precision NULL,
	"yMin" double precision NULL,
	"yMax" double precision NULL,
	"zMin" double precision NULL,
	"zMax" double precision NULL,
	"factionID" int NULL,
	radius double precision NULL,
	PRIMARY KEY ("regionID")
);