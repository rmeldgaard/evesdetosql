﻿-- ----------------------------
-- Table structure for table "ramActivities"
-- ----------------------------

DROP TABLE IF EXISTS "public"."ramActivities";

CREATE TABLE "public"."ramActivities"(
	"activityID" smallint NOT NULL,
	"activityName" varchar(100) NULL,
	"iconNo" varchar(5) NULL,
	description varchar(1000) NULL,
	published bool NULL,
	PRIMARY KEY ("activityID")
);