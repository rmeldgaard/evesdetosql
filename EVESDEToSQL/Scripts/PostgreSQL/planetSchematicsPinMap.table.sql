﻿-- ----------------------------
-- Table structure for table "planetSchematicsPinMap"
-- ----------------------------

DROP TABLE IF EXISTS "public"."planetSchematicsPinMap";

CREATE TABLE "public"."planetSchematicsPinMap"(
	"schematicID" smallint NOT NULL,
	"pinTypeID" int NOT NULL,
	PRIMARY KEY ("schematicID", "pinTypeID")
);