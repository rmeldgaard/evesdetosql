﻿-- ----------------------------
-- Table structure for table "ramTypeRequirements"
-- ----------------------------

DROP TABLE IF EXISTS "public"."staOperationServices";

CREATE TABLE "public"."staOperationServices"(
	"operationID" smallint NOT NULL,
	"serviceID" int NOT NULL,
	PRIMARY KEY ("operationID", "serviceID")
);