﻿-- ----------------------------
-- Table structure for table "mapLandmarks"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapLandmarks";

CREATE TABLE "public"."mapLandmarks"(
	"landmarkID" smallint NOT NULL,
	"landmarkName" varchar(100) NULL,
	description varchar(7000) NULL,
	"locationID" int NULL,
	x double precision NULL,
	y double precision NULL,
	z double precision NULL,
	radius double precision NULL,
	"iconID" int NULL,
	importance smallint NULL,
	PRIMARY KEY ("landmarkID")
);