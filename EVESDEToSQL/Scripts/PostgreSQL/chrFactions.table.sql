﻿-- ----------------------------
-- Table structure for table "chrFactions"
-- ----------------------------

DROP TABLE IF EXISTS "public"."chrFactions";

CREATE TABLE "public"."chrFactions"(
	"factionID" int NOT NULL,
	"factionName" varchar(100) NULL,
	description varchar(1000) NULL,
	"raceIDs" int NULL,
	"solarSystemID" int NULL,
	"corporationID" int NULL,
	"sizeFactor" double precision NULL,
	"stationCount" smallint NULL,
	"stationSystemCount" smallint NULL,
	"militiaCorporationID" int NULL,
	"iconID" int NULL,
    PRIMARY KEY ("factionID")
);