﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmAttributeTypes;

CREATE TABLE sqlite_default_schema.dgmAttributeTypes(
	attributeID smallint NOT NULL,
	attributeName varchar(100) NULL,
	description varchar(1000) NULL,
	iconID int NULL,
	defaultValue float NULL,
	published bit NULL,
	displayName varchar(100) NULL,
	unitID tinyint NULL,
	stackable bit NULL,
	highIsGood bit NULL,
	categoryID tinyint NULL,
	PRIMARY KEY (attributeID ASC)
);