﻿DROP TABLE IF EXISTS sqlite_default_schema.staStations;

CREATE TABLE sqlite_default_schema.staStations(
	stationID int NOT NULL,
	security smallint NULL,
	dockingCostPerVolume float NULL,
	maxShipVolumeDockable float NULL,
	officeRentalCost int NULL,
	operationID tinyint NULL,
	stationTypeID int NULL,
	corporationID int NULL,
	solarSystemID int NULL,
	constellationID int NULL,
	regionID int NULL,
	stationName nvarchar(100) NULL,
	x float NULL,
	y float NULL,
	z float NULL,
	reprocessingEfficiency float NULL,
	reprocessingStationsTake float NULL,
	reprocessingHangarFlag tinyint NULL,
	PRIMARY KEY (stationID ASC)
);

CREATE INDEX staStations_IX_constellation ON sqlite_default_schema.staStations (corporationID ASC);

CREATE INDEX staStations_IX_corporation ON sqlite_default_schema.staStations (corporationID ASC);

CREATE INDEX staStations_IX_operation ON sqlite_default_schema.staStations (operationID ASC);

CREATE INDEX staStations_IX_region ON sqlite_default_schema.staStations (regionID ASC);

CREATE INDEX staStations_IX_system ON sqlite_default_schema.staStations (solarSystemID ASC);

CREATE INDEX staStations_IX_type ON sqlite_default_schema.staStations (stationTypeID ASC);