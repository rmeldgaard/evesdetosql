﻿DROP TABLE IF EXISTS sqlite_default_schema.planetSchematicsPinMap;

CREATE TABLE sqlite_default_schema.planetSchematicsPinMap(
	schematicID smallint NOT NULL,
	pinTypeID int NOT NULL,
	PRIMARY KEY (schematicID ASC, pinTypeID ASC)
);