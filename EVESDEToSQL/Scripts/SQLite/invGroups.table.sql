﻿DROP TABLE IF EXISTS sqlite_default_schema.invGroups;

CREATE TABLE sqlite_default_schema.invGroups(
	groupID int NOT NULL,
	categoryID int NULL,
	groupName nvarchar(100) NULL,
	description nvarchar(3000) NULL,
	iconID int NULL,
	useBasePrice bit NULL,
	allowManufacture bit NULL,
	allowRecycler bit NULL,
	anchored bit NULL,
	anchorable bit NULL,
	fittableNonSingleton bit NULL,
	published bit NULL,
	PRIMARY KEY (groupID ASC)
);

CREATE INDEX invGroups_IX_category ON sqlite_default_schema.invGroups (categoryID ASC);