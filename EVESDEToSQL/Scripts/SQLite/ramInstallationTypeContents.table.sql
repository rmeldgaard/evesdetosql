﻿DROP TABLE IF EXISTS sqlite_default_schema.ramInstallationTypeContents;

CREATE TABLE sqlite_default_schema.ramInstallationTypeContents(
	installationTypeID int NOT NULL,
	assemblyLineTypeID tinyint NOT NULL,
	quantity tinyint NULL,
	PRIMARY KEY (installationTypeID ASC, assemblyLineTypeID ASC)
);