﻿DROP TABLE IF EXISTS sqlite_default_schema.mapSolarSystemJumps;

CREATE TABLE sqlite_default_schema.mapSolarSystemJumps(
	fromRegionID int NULL,
	fromConstellationID int NULL,
	fromSolarSystemID int NOT NULL,
	toSolarSystemID int NOT NULL,
	toConstellationID int NULL,
	toRegionID int NULL,
	PRIMARY KEY (fromSolarSystemID ASC, toSolarSystemID ASC)
);

CREATE INDEX mapSolarSystemJumps_IX_fromConstellation ON sqlite_default_schema.mapSolarSystemJumps (fromConstellationID ASC);

CREATE INDEX mapSolarSystemJumps_IX_fromRegion ON sqlite_default_schema.mapSolarSystemJumps (fromRegionID ASC);