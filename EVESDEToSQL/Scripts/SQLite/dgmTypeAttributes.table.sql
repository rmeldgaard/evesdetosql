﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmTypeAttributes;

CREATE TABLE sqlite_default_schema.dgmTypeAttributes(
	typeID int NOT NULL,
	attributeID smallint NOT NULL,
	valueInt int NULL,
	valueFloat float NULL,
	PRIMARY KEY (typeID ASC, attributeID ASC)
);