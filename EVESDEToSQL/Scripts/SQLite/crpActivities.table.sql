﻿DROP TABLE IF EXISTS sqlite_default_schema.crpActivities;

CREATE TABLE sqlite_default_schema.crpActivities(
	activityID tinyint NOT NULL,
	activityName nvarchar(100) NULL,
	description nvarchar(1000) NULL,
	PRIMARY KEY (activityID ASC)
);