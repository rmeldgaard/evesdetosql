﻿DROP TABLE IF EXISTS sqlite_default_schema.ramAssemblyLineTypes;

CREATE TABLE sqlite_default_schema.ramAssemblyLineTypes(
	assemblyLineTypeID tinyint NOT NULL,
	assemblyLineTypeName nvarchar(100) NULL,
	description nvarchar(1000) NULL,
	baseTimeMultiplier float NULL,
	baseMaterialMultiplier float NULL,
	baseCostMultiplier float NULL,
	volume float NULL,
	activityID tinyint NULL,
	minCostPerHour float NULL,
	PRIMARY KEY (assemblyLineTypeID ASC)
);