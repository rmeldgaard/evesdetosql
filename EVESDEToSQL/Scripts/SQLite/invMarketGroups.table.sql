﻿DROP TABLE IF EXISTS sqlite_default_schema.invMarketGroups;

CREATE TABLE sqlite_default_schema.invMarketGroups(
	marketGroupID int NOT NULL,
	parentGroupID int NULL,
	marketGroupName nvarchar(100) NULL,
	description nvarchar(3000) NULL,
	iconID int NULL,
	hasTypes bit NULL,
	PRIMARY KEY (marketGroupID ASC)
);