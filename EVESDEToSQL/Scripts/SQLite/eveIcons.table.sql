﻿DROP TABLE IF EXISTS sqlite_default_schema.eveIcons;

CREATE TABLE sqlite_default_schema.eveIcons(
	iconID int NOT NULL,
	iconFile varchar(500) NOT NULL DEFAULT '',
	description nvarchar(8000) NOT NULL DEFAULT '',
	PRIMARY KEY (iconID ASC)
);