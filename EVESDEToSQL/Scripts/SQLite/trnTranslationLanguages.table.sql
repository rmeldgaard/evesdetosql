﻿DROP TABLE IF EXISTS sqlite_default_schema.trnTranslationLanguages;

CREATE TABLE sqlite_default_schema.trnTranslationLanguages(
	numericLanguageID int NOT NULL,
	languageID varchar(50) NULL,
	languageName nvarchar(200) NULL,
	PRIMARY KEY (numericLanguageID ASC)
);