﻿DROP TABLE IF EXISTS sqlite_default_schema.chrBloodlines;

CREATE TABLE sqlite_default_schema.chrBloodlines(
	bloodlineID tinyint NOT NULL,
	bloodlineName nvarchar(100) NULL,
	raceID tinyint NULL,
	description nvarchar(1000) NULL,
	maleDescription nvarchar(1000) NULL,
	femaleDescription nvarchar(1000) NULL,
	shipTypeID int NULL,
	corporationID int NULL,
	perception tinyint NULL,
	willpower tinyint NULL,
	charisma tinyint NULL,
	memory tinyint NULL,
	intelligence tinyint NULL,
	iconID int NULL,
	shortDescription nvarchar(500) NULL,
	shortMaleDescription nvarchar(500) NULL,
	shortFemaleDescription nvarchar(500) NULL,
	PRIMARY KEY (bloodlineID ASC)
);