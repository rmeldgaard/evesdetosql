﻿DROP TABLE IF EXISTS sqlite_default_schema.mapDenormalize;

CREATE TABLE sqlite_default_schema.mapDenormalize(
	itemID int NOT NULL,
	typeID int NULL,
	groupID int NULL,
	solarSystemID int NULL,
	constellationID int NULL,
	regionID int NULL,
	orbitID int NULL,
	x float NULL,
	y float NULL,
	z float NULL,
	radius float NULL,
	itemName nvarchar(100) NULL,
	security float NULL,
	celestialIndex tinyint NULL,
	orbitIndex tinyint NULL,
	PRIMARY KEY (itemID ASC)
);

CREATE INDEX mapDenormalize_IX_constellation ON sqlite_default_schema.mapDenormalize (constellationID ASC);

CREATE INDEX mapDenormalize_IX_groupConstellation ON sqlite_default_schema.mapDenormalize (groupID ASC, constellationID ASC);

CREATE INDEX mapDenormalize_IX_groupRegion ON sqlite_default_schema.mapDenormalize (groupID ASC, regionID ASC);

CREATE INDEX mapDenormalize_IX_groupSystem ON sqlite_default_schema.mapDenormalize (groupID ASC, solarSystemID ASC);

CREATE INDEX mapDenormalize_IX_orbit ON sqlite_default_schema.mapDenormalize (orbitID ASC);

CREATE INDEX mapDenormalize_IX_region ON sqlite_default_schema.mapDenormalize (regionID ASC);

CREATE INDEX mapDenormalize_IX_system ON sqlite_default_schema.mapDenormalize (solarSystemID ASC);