﻿DROP TABLE IF EXISTS sqlite_default_schema.warCombatZoneSystems;

CREATE TABLE sqlite_default_schema.warCombatZoneSystems(
	solarSystemID int NOT NULL,
	combatZoneID int NULL,
	PRIMARY KEY (solarSystemID ASC)
);