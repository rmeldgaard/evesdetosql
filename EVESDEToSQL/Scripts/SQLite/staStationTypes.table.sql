﻿DROP TABLE IF EXISTS sqlite_default_schema.staStationTypes;

CREATE TABLE sqlite_default_schema.staStationTypes(
	stationTypeID int NOT NULL,
	dockEntryX float NULL,
	dockEntryY float NULL,
	dockEntryZ float NULL,
	dockOrientationX float NULL,
	dockOrientationY float NULL,
	dockOrientationZ float NULL,
	operationID tinyint NULL,
	officeSlots tinyint NULL,
	reprocessingEfficiency float NULL,
	conquerable bit NULL,
	PRIMARY KEY (stationTypeID ASC)
);