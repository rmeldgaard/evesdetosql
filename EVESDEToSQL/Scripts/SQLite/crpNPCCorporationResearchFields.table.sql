﻿DROP TABLE IF EXISTS sqlite_default_schema.crpNPCCorporationResearchFields;

CREATE TABLE sqlite_default_schema.crpNPCCorporationResearchFields(
	skillID int NOT NULL,
	corporationID int NOT NULL,
	PRIMARY KEY (skillID ASC, corporationID ASC)
);