﻿DROP TABLE IF EXISTS sqlite_default_schema.warCombatZones;

CREATE TABLE sqlite_default_schema.warCombatZones(
	combatZoneID int NOT NULL DEFAULT -1,
	combatZoneName nvarchar(100) NULL,
	factionID int NULL,
	centerSystemID int NULL,
	description nvarchar(500) NULL,
	PRIMARY KEY (combatZoneID ASC)
);