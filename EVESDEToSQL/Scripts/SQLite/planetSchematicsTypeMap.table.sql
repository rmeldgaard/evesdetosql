﻿DROP TABLE IF EXISTS sqlite_default_schema.planetSchematicsTypeMap;

CREATE TABLE sqlite_default_schema.planetSchematicsTypeMap(
	schematicID smallint NOT NULL,
	typeID int NOT NULL,
	quantity smallint NULL,
	isInput bit NULL,
	PRIMARY KEY (schematicID ASC, typeID ASC)
);