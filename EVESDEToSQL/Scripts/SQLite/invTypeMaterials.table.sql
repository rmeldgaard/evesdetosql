﻿DROP TABLE IF EXISTS sqlite_default_schema.invTypeMaterials;

CREATE TABLE sqlite_default_schema.invTypeMaterials(
	typeID int NOT NULL,
	materialTypeID int NOT NULL,
	quantity int NOT NULL DEFAULT 0,
	PRIMARY KEY (typeID ASC, materialTypeID ASC)
);