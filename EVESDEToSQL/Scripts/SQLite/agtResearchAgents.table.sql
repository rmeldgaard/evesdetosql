﻿DROP TABLE IF EXISTS sqlite_default_schema.agtResearchAgents;

CREATE TABLE sqlite_default_schema.agtResearchAgents(
	agentID int NOT NULL,
	typeID int NOT NULL,
	PRIMARY KEY (agentID ASC, typeID ASC)
);

CREATE INDEX agtResearchAgents_IX_type ON sqlite_default_schema.agtResearchAgents (typeID ASC);