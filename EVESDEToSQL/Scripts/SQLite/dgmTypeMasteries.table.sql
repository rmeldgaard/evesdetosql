﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmTypeMasteries;

CREATE TABLE sqlite_default_schema.dgmTypeMasteries(
	typeID int NOT NULL,
	masteryID smallint NOT NULL,
	PRIMARY KEY (typeID ASC, masteryID ASC)
);