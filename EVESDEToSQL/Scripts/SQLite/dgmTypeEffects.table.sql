﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmTypeEffects;

CREATE TABLE sqlite_default_schema.dgmTypeEffects(
	typeID int NOT NULL,
	effectID smallint NOT NULL,
	isDefault bit NULL,
	PRIMARY KEY (typeID ASC, effectID ASC)
);