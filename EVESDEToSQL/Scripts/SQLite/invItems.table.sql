﻿DROP TABLE IF EXISTS sqlite_default_schema.invItems;

CREATE TABLE sqlite_default_schema.invItems(
	itemID bigint NOT NULL,
	typeID int NOT NULL,
	ownerID int NOT NULL,
	locationID bigint NOT NULL,
	flagID smallint NOT NULL,
	quantity int NOT NULL,
	PRIMARY KEY (itemID ASC)
);

CREATE INDEX items_IX_Location ON sqlite_default_schema.invItems (locationID ASC);

CREATE INDEX items_IX_OwnerLocation ON sqlite_default_schema.invItems (ownerID ASC, locationID ASC);