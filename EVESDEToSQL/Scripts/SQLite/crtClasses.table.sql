DROP TABLE IF EXISTS sqlite_default_schema.crtClasses;

CREATE TABLE sqlite_default_schema.crtClasses(
	classID int NOT NULL,
	description nvarchar(500) NULL,
	className nvarchar(256) NULL,
	PRIMARY KEY (classID ASC)
);