﻿DROP TABLE IF EXISTS sqlite_default_schema.ramAssemblyLineTypeDetailPerGroup;

CREATE TABLE sqlite_default_schema.ramAssemblyLineTypeDetailPerGroup(
	assemblyLineTypeID tinyint NOT NULL,
	groupID int NOT NULL,
	timeMultiplier float NULL,
	materialMultiplier float NULL,
	costMultiplier float NULL,
	PRIMARY KEY (assemblyLineTypeID ASC, groupID ASC)
);