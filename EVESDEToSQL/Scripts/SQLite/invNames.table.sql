﻿DROP TABLE IF EXISTS sqlite_default_schema.invNames;

CREATE TABLE sqlite_default_schema.invNames(
	itemID bigint NOT NULL,
	itemName nvarchar(200) NOT NULL,
	PRIMARY KEY (itemID ASC)
);