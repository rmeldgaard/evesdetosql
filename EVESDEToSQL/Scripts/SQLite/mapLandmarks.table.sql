﻿DROP TABLE IF EXISTS sqlite_default_schema.mapLandmarks;

CREATE TABLE sqlite_default_schema.mapLandmarks(
	landmarkID smallint NOT NULL,
	landmarkName varchar(100) NULL,
	description varchar(7000) NULL,
	locationID int NULL,
	x float NULL,
	y float NULL,
	z float NULL,
	radius float NULL,
	iconID int NULL,
	importance tinyint NULL,
	PRIMARY KEY (landmarkID ASC)
);