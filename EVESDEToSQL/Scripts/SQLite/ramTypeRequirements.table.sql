﻿DROP TABLE IF EXISTS sqlite_default_schema.ramTypeRequirements;

CREATE TABLE sqlite_default_schema.ramTypeRequirements(
	typeID int NOT NULL,
	activityID tinyint NOT NULL,
	requiredTypeID int NOT NULL,
	quantity int NULL,
	level int NULL,
	damagePerJob float NULL,
	recycle bit NULL,
	raceID int NULL,
	probability float NULL,
	consume bit NULL,
	PRIMARY KEY (typeID ASC, activityID ASC, requiredTypeID ASC)
);