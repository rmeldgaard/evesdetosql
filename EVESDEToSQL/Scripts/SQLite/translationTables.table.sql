﻿DROP TABLE IF EXISTS sqlite_default_schema.translationTables;

CREATE TABLE sqlite_default_schema.translationTables(
	sourceTable nvarchar(200) NOT NULL,
	destinationTable nvarchar(200) NULL,
	translatedKey nvarchar(200) NOT NULL,
	tcGroupID int NULL,
	tcID int NULL,
	PRIMARY KEY (sourceTable ASC, translatedKey ASC)
);