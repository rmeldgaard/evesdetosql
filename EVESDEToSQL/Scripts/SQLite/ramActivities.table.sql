﻿DROP TABLE IF EXISTS sqlite_default_schema.ramActivities;

CREATE TABLE sqlite_default_schema.ramActivities(
	activityID tinyint NOT NULL,
	activityName nvarchar(100) NULL,
	iconNo varchar(5) NULL,
	description nvarchar(1000) NULL,
	published bit NULL,
	PRIMARY KEY (activityID ASC)
);