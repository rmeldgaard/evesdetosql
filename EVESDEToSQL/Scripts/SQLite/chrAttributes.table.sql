﻿DROP TABLE IF EXISTS sqlite_default_schema.chrAttributes;

CREATE TABLE sqlite_default_schema.chrAttributes(
	attributeID tinyint NOT NULL,
	attributeName varchar(100) NULL,
	description varchar(1000) NULL,
	iconID int NULL,
	shortDescription nvarchar(500) NULL,
	notes nvarchar(500) NULL,
	PRIMARY KEY (attributeID ASC)
);