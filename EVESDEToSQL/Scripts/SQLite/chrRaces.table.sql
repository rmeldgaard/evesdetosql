﻿DROP TABLE IF EXISTS sqlite_default_schema.chrRaces;

CREATE TABLE sqlite_default_schema.chrRaces(
	raceID tinyint NOT NULL,
	raceName varchar(100) NULL,
	description varchar(1000) NULL,
	iconID int NULL,
	shortDescription varchar(500) NULL,
    PRIMARY KEY (raceID ASC)
);