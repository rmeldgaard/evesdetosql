--
-- Table structure for table `crtClasses`
--

DROP TABLE IF EXISTS `crtClasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crtClasses` (
	`classID` INT(10) unsigned NOT NULL,
	`description` NVARCHAR(500) NULL,
	`className` NVARCHAR(256) NULL,
	PRIMARY KEY (`classID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crtClasses`
--

LOCK TABLES `crtClasses` WRITE;
/*!40000 ALTER TABLE `crtClasses` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crtClasses` ENABLE KEYS */;
UNLOCK TABLES;