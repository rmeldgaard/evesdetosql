﻿--
-- Table structure for table `dgmMasteries`
--

DROP TABLE IF EXISTS `dgmMasteries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmMasteries` (
	`masteryID` INT(10) unsigned NOT NULL,
	`certificateID` INT(10) unsigned NOT NULL,
	`grade` TINYINT(3) NOT NULL,
	PRIMARY KEY (`masteryID`),
	UNIQUE `dgmMasteries_UC_certificate_grade` (`certificateID` ASC, `grade` ASC),
	KEY `dgmMasteries_IX_certificate` (`certificateID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmMasteries`
--

LOCK TABLES `dgmMasteries` WRITE;
/*!40000 ALTER TABLE `dgmMasteries` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmMasteries` ENABLE KEYS */;
UNLOCK TABLES;