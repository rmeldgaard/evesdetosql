--
-- Table structure for table `crtRecommendations`
--

DROP TABLE IF EXISTS `crtRecommendations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crtRecommendations` (
	`recommendationID` INT(10) unsigned NOT NULL,
	`shipTypeID` INT(10) unsigned NULL,
	`certificateID` INT(10) unsigned NULL,
	`recommendationLevel` TINYINT(3) NOT NULL,
	PRIMARY KEY (`recommendationID` ASC),
	KEY `crtRecommendations_IX_certificate` (`certificateID` ASC),
	KEY `crtRecommendations_IX_shipType` (`shipTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crtRecommendations`
--

LOCK TABLES `crtRecommendations` WRITE;
/*!40000 ALTER TABLE `crtRecommendations` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crtRecommendations` ENABLE KEYS */;
UNLOCK TABLES;