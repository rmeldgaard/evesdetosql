﻿--
-- Table structure for table `translationTables`
--

DROP TABLE IF EXISTS `translationTables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translationTables` (
	`sourceTable` NVARCHAR(200) NOT NULL,
	`destinationTable` NVARCHAR(200) NULL,
	`translatedKey` NVARCHAR(200) NOT NULL,
	`tcGroupID` INT(10) unsigned NULL,
	`tcID` INT(10) unsigned NULL,
	PRIMARY KEY (`sourceTable` ASC, `translatedKey` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translationTables`
--

LOCK TABLES `translationTables` WRITE;
/*!40000 ALTER TABLE `translationTables` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `translationTables` ENABLE KEYS */;
UNLOCK TABLES;