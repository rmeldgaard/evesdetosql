﻿--
-- Table structure for table `ramAssemblyLineTypes`
--

DROP TABLE IF EXISTS `ramAssemblyLineTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramAssemblyLineTypes` (
	`assemblyLineTypeID` TINYINT(3) unsigned NOT NULL,
	`assemblyLineTypeName` NVARCHAR(100) NULL,
	`description` NVARCHAR(1000) NULL,
	`baseTimeMultiplier` DOUBLE NULL,
	`baseMaterialMultiplier` DOUBLE NULL,
	`baseCostMultiplier` DOUBLE NULL,
	`volume` DOUBLE NULL,
	`activityID` TINYINT(3) unsigned NULL,
	`minCostPerHour` DOUBLE NULL,
	PRIMARY KEY (`assemblyLineTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ramAssemblyLineTypes`
--

LOCK TABLES `ramAssemblyLineTypes` WRITE;
/*!40000 ALTER TABLE `ramAssemblyLineTypes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `ramAssemblyLineTypes` ENABLE KEYS */;
UNLOCK TABLES;