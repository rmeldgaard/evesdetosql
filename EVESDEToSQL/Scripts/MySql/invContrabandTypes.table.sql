﻿--
-- Table structure for table `invContrabandTypes`
--

DROP TABLE IF EXISTS `invContrabandTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invContrabandTypes` (
	`factionID` INT(10) unsigned NOT NULL,
	`typeID` INT(10) unsigned NOT NULL,
	`standingLoss` DOUBLE NULL,
	`confiscateMinSec` DOUBLE NULL,
	`fineByValue` DOUBLE NULL,
	`attackMinSec` DOUBLE NULL,
	PRIMARY KEY (`factionID` ASC, `typeID` ASC),
	KEY `invContrabandTypes_IX_type` (`typeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invContrabandTypes`
--

LOCK TABLES `invContrabandTypes` WRITE;
/*!40000 ALTER TABLE `invContrabandTypes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invContrabandTypes` ENABLE KEYS */;
UNLOCK TABLES;