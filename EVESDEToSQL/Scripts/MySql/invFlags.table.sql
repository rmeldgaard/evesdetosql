﻿--
-- Table structure for table `invFlags`
--

DROP TABLE IF EXISTS `invFlags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invFlags` (
	`flagID` SMALLINT(6) unsigned NOT NULL,
	`flagName` VARCHAR(200) NULL,
	`flagText` VARCHAR(100) NULL,
	`orderID` INT(10) unsigned NULL,
	PRIMARY KEY (`flagID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invFlags`
--

LOCK TABLES `invFlags` WRITE;
/*!40000 ALTER TABLE `invFlags` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invFlags` ENABLE KEYS */;
UNLOCK TABLES;