﻿--
-- Table structure for table `dgmAttributeCategories`
--

DROP TABLE IF EXISTS `dgmAttributeCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmAttributeCategories` (
	`categoryID` TINYINT(3) unsigned NOT NULL,
	`categoryName` NVARCHAR(50) NULL,
	`categoryDescription` NVARCHAR(200) NULL,
	PRIMARY KEY (`categoryID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmAttributeCategories`
--

LOCK TABLES `dgmAttributeCategories` WRITE;
/*!40000 ALTER TABLE `dgmAttributeCategories` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmAttributeCategories` ENABLE KEYS */;
UNLOCK TABLES;