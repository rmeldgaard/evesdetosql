﻿--
-- Table structure for table `crpNPCCorporations`
--

DROP TABLE IF EXISTS `crpNPCCorporations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crpNPCCorporations` (
	`corporationID` INT(10) unsigned NOT NULL,
	`size` CHAR NULL,
	`extent` CHAR NULL,
	`solarSystemID` INT(10) unsigned NULL,
	`investorID1` INT(10) unsigned NULL,
	`investorShares1` TINYINT(3) NULL,
	`investorID2` INT(10) unsigned NULL,
	`investorShares2` TINYINT(3) NULL,
	`investorID3` INT(10) unsigned NULL,
	`investorShares3` TINYINT(3) NULL,
	`investorID4` INT(10) unsigned NULL,
	`investorShares4` TINYINT(3) NULL,
	`friendID` INT(10) unsigned NULL,
	`enemyID` INT(10) unsigned NULL,
	`publicShares` BIGINT(20) NULL,
	`initialPrice` INT(10) NULL,
	`minSecurity` DOUBLE NULL,
	`scattered` TINYINT(1) NULL,
	`fringe` TINYINT(3) NULL,
	`corridor` TINYINT(3) NULL,
	`hub` TINYINT(3) NULL,
	`border` TINYINT(3) NULL,
	`factionID` INT(10) unsigned NULL,
	`sizeFactor` DOUBLE NULL,
	`stationCount` SMALLINT(6) NULL,
	`stationSystemCount` SMALLINT(6) NULL,
	`description` NVARCHAR(4000) NULL,
	`iconID` INT(10) unsigned NULL,
	PRIMARY KEY (`corporationID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crpNPCCorporations`
--

LOCK TABLES `crpNPCCorporations` WRITE;
/*!40000 ALTER TABLE `crpNPCCorporations` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crpNPCCorporations` ENABLE KEYS */;
UNLOCK TABLES;