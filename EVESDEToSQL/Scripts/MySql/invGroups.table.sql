﻿--
-- Table structure for table `invGroups`
--

DROP TABLE IF EXISTS `invGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invGroups` (
	`groupID` INT(10) unsigned NOT NULL,
	`categoryID` INT(10) unsigned NULL,
	`groupName` NVARCHAR(100) NULL,
	`description` NVARCHAR(3000) NULL,
	`iconID` INT(10) unsigned NULL,
	`useBasePrice` TINYINT(1) NULL,
	`allowManufacture` TINYINT(1) NULL,
	`allowRecycler` TINYINT(1) NULL,
	`anchored` TINYINT(1) NULL,
	`anchorable` TINYINT(1) NULL,
	`fittableNonSingleton` TINYINT(1) NULL,
	`published` TINYINT(1) NULL,
	PRIMARY KEY (`groupID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invGroups`
--

LOCK TABLES `invGroups` WRITE;
/*!40000 ALTER TABLE `invGroups` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invGroups` ENABLE KEYS */;
UNLOCK TABLES;