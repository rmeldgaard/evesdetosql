﻿--
-- Table structure for table `invMetaTypes`
--

DROP TABLE IF EXISTS `invMetaTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invMetaTypes` (
	`typeID` INT(10) unsigned NOT NULL,
	`parentTypeID` INT(10) unsigned NULL,
	`metaGroupID` SMALLINT(6) unsigned NULL,
	PRIMARY KEY (`typeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invMetaTypes`
--

LOCK TABLES `invMetaTypes` WRITE;
/*!40000 ALTER TABLE `invMetaTypes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invMetaTypes` ENABLE KEYS */;
UNLOCK TABLES;