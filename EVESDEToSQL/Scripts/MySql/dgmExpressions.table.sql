﻿--
-- Table structure for table `dgmExpressions`
--

DROP TABLE IF EXISTS `dgmExpressions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmExpressions` (
	`expressionID` INT(10) unsigned NOT NULL,
	`operandID` INT(10) unsigned NULL,
	`arg1` INT(10) unsigned NULL,
	`arg2` INT(10) NULL,
	`expressionValue` VARCHAR(100) NULL,
	`description` VARCHAR(1000) NULL,
	`expressionName` VARCHAR(500) NULL,
	`expressionTypeID` INT(10) unsigned NULL,
	`expressionGroupID` SMALLINT(6) unsigned NULL,
	`expressionAttributeID` SMALLINT(6) unsigned NULL,
	PRIMARY KEY (`expressionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmExpressions`
--

LOCK TABLES `dgmExpressions` WRITE;
/*!40000 ALTER TABLE `dgmExpressions` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmExpressions` ENABLE KEYS */;
UNLOCK TABLES;