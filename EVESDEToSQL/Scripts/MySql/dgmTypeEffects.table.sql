﻿--
-- Table structure for table `dgmTypeEffects`
--

DROP TABLE IF EXISTS `dgmTypeEffects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmTypeEffects` (
	`typeID` INT(10) unsigned NOT NULL,
	`effectID` SMALLINT(6) unsigned NOT NULL,
	`isDefault` TINYINT(1) NULL,
	PRIMARY KEY (`typeID` ASC, `effectID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmTypeEffects`
--

LOCK TABLES `dgmTypeEffects` WRITE;
/*!40000 ALTER TABLE `dgmTypeEffects` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmTypeEffects` ENABLE KEYS */;
UNLOCK TABLES;