﻿--
-- Table structure for table `invUniqueNames`
--

DROP TABLE IF EXISTS `invUniqueNames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invUniqueNames` (
	`itemID` INT(10) unsigned NOT NULL,
	`itemName` NVARCHAR(200) NOT NULL,
	`groupID` INT(10) unsigned NULL,
	PRIMARY KEY (`itemID` ASC),
	KEY `invUniqueNames_IX_GroupName` (`groupID` ASC, `itemName` ASC),
	KEY `invUniqueNames_UQ` (`itemName` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invUniqueNames`
--

LOCK TABLES `invUniqueNames` WRITE;
/*!40000 ALTER TABLE `invUniqueNames` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invUniqueNames` ENABLE KEYS */;
UNLOCK TABLES;