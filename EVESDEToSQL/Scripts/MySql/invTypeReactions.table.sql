﻿--
-- Table structure for table `invTypeReactions`
--

DROP TABLE IF EXISTS `invTypeReactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invTypeReactions` (
	`reactionTypeID` INT(10) unsigned NOT NULL,
	`input` TINYINT(1) NOT NULL,
	`typeID` INT(10) unsigned NOT NULL,
	`quantity` SMALLINT(6) NULL,
	PRIMARY KEY (`reactionTypeID` ASC, `input` ASC,	`typeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invTypeReactions`
--

LOCK TABLES `invTypeReactions` WRITE;
/*!40000 ALTER TABLE `invTypeReactions` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invTypeReactions` ENABLE KEYS */;
UNLOCK TABLES;