﻿--
-- Table structure for table `mapDenormalize`
--

DROP TABLE IF EXISTS `mapDenormalize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapDenormalize` (
	`itemID` INT(10) unsigned NOT NULL,
	`typeID` INT(10) unsigned NULL,
	`groupID` INT(10) unsigned NULL,
	`solarSystemID` INT(10) unsigned NULL,
	`constellationID` INT(10) unsigned NULL,
	`regionID` INT(10) unsigned NULL,
	`orbitID` INT(10) unsigned NULL,
	`x` DOUBLE NULL,
	`y` DOUBLE NULL,
	`z` DOUBLE NULL,
	`radius` DOUBLE NULL,
	`itemName` NVARCHAR(100) NULL,
	`security` DOUBLE NULL,
	`celestialIndex` TINYINT(3) NULL,
	`orbitIndex` TINYINT(3) NULL,
	PRIMARY KEY (`itemID` ASC),
	KEY `mapDenormalize_IX_constellation` (`constellationID` ASC),
	KEY `mapDenormalize_IX_groupConstellation` (`groupID` ASC, `constellationID` ASC),
	KEY `mapDenormalize_IX_groupRegion` (`groupID` ASC, `regionID` ASC),
	KEY `mapDenormalize_IX_groupSystem` (`groupID` ASC, `solarSystemID` ASC),
	KEY `mapDenormalize_IX_orbit` (`orbitID` ASC),
	KEY `mapDenormalize_IX_region` (`regionID` ASC),
	KEY `mapDenormalize_IX_system` (`solarSystemID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapDenormalize`
--

LOCK TABLES `mapDenormalize` WRITE;
/*!40000 ALTER TABLE `mapDenormalize` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapDenormalize` ENABLE KEYS */;
UNLOCK TABLES;