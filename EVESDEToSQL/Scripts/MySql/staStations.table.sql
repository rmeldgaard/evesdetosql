﻿--
-- Table structure for table `staStations`
--

DROP TABLE IF EXISTS `staStations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staStations` (
	`stationID` INT(10) unsigned NOT NULL,
	`security` SMALLINT(6) NULL,
	`dockingCostPerVolume` DOUBLE NULL,
	`maxShipVolumeDockable` DOUBLE NULL,
	`officeRentalCost` INT(10) NULL,
	`operationID` TINYINT(3) unsigned NULL,
	`stationTypeID` INT(10) unsigned NULL,
	`corporationID` INT(10) unsigned NULL,
	`solarSystemID` INT(10) unsigned NULL,
	`constellationID` INT(10) unsigned NULL,
	`regionID` INT(10) unsigned NULL,
	`stationName` NVARCHAR(100) NULL,
	`x` DOUBLE NULL,
	`y` DOUBLE NULL,
	`z` DOUBLE NULL,
	`reprocessingEfficiency` DOUBLE NULL,
	`reprocessingStationsTake` DOUBLE NULL,
	`reprocessingHangarFlag` TINYINT(3) NULL,
	PRIMARY KEY (`stationID` ASC),
	KEY `staStations_IX_constellation` (`corporationID` ASC),
	KEY `staStations_IX_corporation` (`corporationID` ASC),
	KEY `staStations_IX_operation` (`operationID` ASC),
	KEY `staStations_IX_region` (`regionID` ASC),
	KEY `staStations_IX_system` (`solarSystemID` ASC),
	KEY `staStations_IX_type` (`stationTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staStations`
--

LOCK TABLES `staStations` WRITE;
/*!40000 ALTER TABLE `staStations` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `staStations` ENABLE KEYS */;
UNLOCK TABLES;