﻿--
-- Table structure for table `chrFactions`
--

DROP TABLE IF EXISTS `chrFactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chrFactions` (
	`factionID` INT(10) unsigned NOT NULL,
	`factionName` VARCHAR(100) NULL,
	`description` VARCHAR(1000) NULL,
	`raceIDs` INT(10) unsigned NULL,
	`solarSystemID` INT(10) unsigned NULL,
	`corporationID` INT(10) unsigned NULL,
	`sizeFactor` DOUBLE NULL,
	`stationCount` SMALLINT(6) NULL,
	`stationSystemCount` SMALLINT(6) NULL,
	`militiaCorporationID` INT(10) unsigned NULL,
	`iconID` INT(10) unsigned NULL,
    PRIMARY KEY (`factionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chrFactions`
--

LOCK TABLES `chrFactions` WRITE;
/*!40000 ALTER TABLE `chrFactions` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `chrFactions` ENABLE KEYS */;
UNLOCK TABLES;