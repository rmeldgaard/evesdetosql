﻿--
-- Table structure for table `mapCelestialStatistics`
--

DROP TABLE IF EXISTS `mapCelestialStatistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapCelestialStatistics` (
	`celestialID` INT(10) unsigned NOT NULL,
	`temperature` DOUBLE NULL,
	`spectralClass` VARCHAR(10) NULL,
	`luminosity` DOUBLE NULL,
	`age` DOUBLE NULL,
	`life` DOUBLE NULL,
	`orbitRadius` DOUBLE NULL,
	`eccentricity` DOUBLE NULL,
	`massDust` DOUBLE NULL,
	`massGas` DOUBLE NULL,
	`fragmented` TINYINT(1) NULL,
	`density` DOUBLE NULL,
	`surfaceGravity` DOUBLE NULL,
	`escapeVelocity` DOUBLE NULL,
	`orbitPeriod` DOUBLE NULL,
	`rotationRate` DOUBLE NULL,
	`locked` TINYINT(1) NULL,
	`pressure` DOUBLE NULL,
	`radius` DOUBLE NULL,
	`mass` DOUBLE NULL,
	PRIMARY KEY (`celestialID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapCelestialStatistics`
--

LOCK TABLES `mapCelestialStatistics` WRITE;
/*!40000 ALTER TABLE `mapCelestialStatistics` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapCelestialStatistics` ENABLE KEYS */;
UNLOCK TABLES;