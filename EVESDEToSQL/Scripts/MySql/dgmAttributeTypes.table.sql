﻿--
-- Table structure for table `dgmAttributeTypes`
--

DROP TABLE IF EXISTS `dgmAttributeTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmAttributeTypes` (
	`attributeID` SMALLINT(6) unsigned NOT NULL,
	`attributeName` VARCHAR(100) NULL,
	`description` VARCHAR(1000) NULL,
	`iconID` INT(10) unsigned NULL,
	`defaultValue` DOUBLE NULL,
	`published` TINYINT(1) NULL,
	`displayName` VARCHAR(100) NULL,
	`unitID` TINYINT(3) unsigned NULL,
	`stackable` TINYINT(1) NULL,
	`highIsGood` TINYINT(1) NULL,
	`categoryID` TINYINT(3) unsigned NULL,
	PRIMARY KEY (`attributeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmAttributeTypes`
--

LOCK TABLES `dgmAttributeTypes` WRITE;
/*!40000 ALTER TABLE `dgmAttributeTypes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmAttributeTypes` ENABLE KEYS */;
UNLOCK TABLES;