﻿--
-- Table structure for table `invTypeMaterials`
--

DROP TABLE IF EXISTS `invTypeMaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invTypeMaterials` (
	`typeID` INT(10) unsigned NOT NULL,
	`materialTypeID` INT(10) unsigned NOT NULL,
	`quantity` INT(10) NOT NULL DEFAULT 0,
	PRIMARY KEY (`typeID` ASC, `materialTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invTypeMaterials`
--

LOCK TABLES `invTypeMaterials` WRITE;
/*!40000 ALTER TABLE `invTypeMaterials` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invTypeMaterials` ENABLE KEYS */;
UNLOCK TABLES;