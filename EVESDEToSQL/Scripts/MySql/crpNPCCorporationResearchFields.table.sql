﻿--
-- Table structure for table `crpNPCCorporationResearchFields`
--

DROP TABLE IF EXISTS `crpNPCCorporationResearchFields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crpNPCCorporationResearchFields` (
	`skillID` INT(10) unsigned NOT NULL,
	`corporationID` INT(10) unsigned NOT NULL,
	PRIMARY KEY (`skillID` ASC,	`corporationID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crpNPCCorporationResearchFields`
--

LOCK TABLES `crpNPCCorporationResearchFields` WRITE;
/*!40000 ALTER TABLE `crpNPCCorporationResearchFields` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crpNPCCorporationResearchFields` ENABLE KEYS */;
UNLOCK TABLES;