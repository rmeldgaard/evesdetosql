﻿CREATE TABLE [eveUnits](
	[unitID] tinyint NOT NULL,
	[unitName] varchar(100) NULL,
	[displayName] varchar(50) NULL,
	[description] memo NULL,
 CONSTRAINT [eveUnits_PK] PRIMARY KEY ([unitID])
);