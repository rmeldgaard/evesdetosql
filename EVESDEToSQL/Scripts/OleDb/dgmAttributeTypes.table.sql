﻿CREATE TABLE [dgmAttributeTypes](
	[attributeID] smallint NOT NULL,
	[attributeName] varchar(100) NULL,
	[description] memo NULL,
	[iconID] int NULL,
	[defaultValue] double NULL,
	[published] bit NULL,
	[displayName] varchar(100) NULL,
	[unitID] tinyint NULL,
	[stackable] bit NULL,
	[highIsGood] bit NULL,
	[categoryID] tinyint NULL,
 CONSTRAINT [dgmAttributeTypes_PK] PRIMARY KEY ([attributeID])
);