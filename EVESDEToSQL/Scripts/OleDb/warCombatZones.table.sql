﻿CREATE TABLE [warCombatZones](
	[combatZoneID] int NOT NULL DEFAULT -1,
	[combatZoneName] varchar(100) NULL,
	[factionID] int NULL,
	[centerSystemID] int NULL,
	[description] memo NULL,
 CONSTRAINT [warCombatZones_PK] PRIMARY KEY ([combatZoneID])
);