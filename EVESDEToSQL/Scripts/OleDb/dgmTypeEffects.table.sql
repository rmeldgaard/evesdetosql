﻿CREATE TABLE [dgmTypeEffects](
	[typeID] int NOT NULL,
	[effectID] smallint NOT NULL,
	[isDefault] bit NULL,
 CONSTRAINT [dgmTypeEffects_PK] PRIMARY KEY ([typeID], [effectID])
);