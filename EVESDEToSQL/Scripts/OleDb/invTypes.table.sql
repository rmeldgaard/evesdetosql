﻿CREATE TABLE [invTypes](
	[typeID] int NOT NULL,
	[groupID] int NULL,
	[typeName] varchar(100) NULL,
	[description] memo NULL,
	[mass] double NULL,
	[volume] double NULL,
	[capacity] double NULL,
	[portionSize] int NULL,
	[raceID] tinyint NULL,
	[basePrice] currency NULL,
	[published] bit NULL,
	[marketGroupID] int NULL,
	[chanceOfDuplicating] double NULL,
	[factionID] int NULL,
	[graphicID] int NULL,
	[iconID] int NULL,
	[radius] double NULL,
	[soundID] int NULL,
 CONSTRAINT [invTypes_PK] PRIMARY KEY ([typeID])
);

CREATE INDEX [invTypes_IX_Group] ON [invTypes] ([groupID] ASC);