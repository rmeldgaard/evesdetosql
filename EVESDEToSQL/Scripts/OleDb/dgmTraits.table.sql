﻿CREATE TABLE [dgmTraits](
	[traitID] int NOT NULL,
	[bonusText] memo NOT NULL,
	[unitID] tinyint NULL,
 CONSTRAINT [dgmTraits_PK] PRIMARY KEY ([traitID])
);