﻿CREATE TABLE [invMetaTypes](
	[typeID] int NOT NULL,
	[parentTypeID] int NULL,
	[metaGroupID] smallint NULL,
 CONSTRAINT [invMetaTypes_PK] PRIMARY KEY ([typeID])
);