﻿CREATE TABLE [mapRegions](
	[regionID] int NOT NULL,
	[regionName] varchar(100) NULL,
	[x] double NULL,
	[y] double NULL,
	[z] double NULL,
	[xMin] double NULL,
	[xMax] double NULL,
	[yMin] double NULL,
	[yMax] double NULL,
	[zMin] double NULL,
	[zMax] double NULL,
	[factionID] int NULL,
	[radius] double NULL,
 CONSTRAINT [mapRegions_PK] PRIMARY KEY ([regionID])
);