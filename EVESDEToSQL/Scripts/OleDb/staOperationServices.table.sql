﻿CREATE TABLE [staOperationServices](
	[operationID] tinyint NOT NULL,
	[serviceID] int NOT NULL,
 CONSTRAINT [staOperationServices_PK] PRIMARY KEY ([operationID], [serviceID])
);