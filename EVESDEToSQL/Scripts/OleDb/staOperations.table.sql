﻿CREATE TABLE [staOperations](
	[activityID] tinyint NULL,
	[operationID] tinyint NOT NULL,
	[operationName] varchar(100) NULL,
	[description] memo NULL,
	[fringe] tinyint NULL,
	[corridor] tinyint NULL,
	[hub] tinyint NULL,
	[border] tinyint NULL,
	[ratio] tinyint NULL,
	[caldariStationTypeID] int NULL,
	[minmatarStationTypeID] int NULL,
	[amarrStationTypeID] int NULL,
	[gallenteStationTypeID] int NULL,
	[joveStationTypeID] int NULL,
 CONSTRAINT [staOperations_PK] PRIMARY KEY ([operationID])
);