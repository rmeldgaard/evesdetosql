﻿CREATE TABLE [eveGraphics](
	[graphicID] int NOT NULL,
	[graphicFile] memo NOT NULL DEFAULT '',
	[description] memo NOT NULL DEFAULT '',
	[obsolete] bit NOT NULL DEFAULT 0,
	[graphicType] varchar(100) NULL,
	[collidable] bit NULL,
	[directoryID] int NULL,
	[graphicName] varchar(64) NOT NULL DEFAULT '',
	[gfxRaceID] varchar(255) NULL,
	[colorScheme] varchar(255) NULL,
	[sofHullName] varchar(64) NULL,
 CONSTRAINT [eveGraphics_PK] PRIMARY KEY ([graphicID])
);