﻿CREATE TABLE [crpActivities](
	[activityID] tinyint NOT NULL,
	[activityName] varchar(100) NULL,
	[description] memo NULL,
 CONSTRAINT [crpActivities_PK] PRIMARY KEY ([activityID])
);