﻿CREATE TABLE [chrAttributes](
	[attributeID] tinyint NOT NULL,
	[attributeName] varchar(100) NULL,
	[description] memo NULL,
	[iconID] int NULL,
	[shortDescription] memo NULL,
	[notes] memo NULL,
 CONSTRAINT [chrAttributes_PK] PRIMARY KEY ([attributeID])
);