﻿CREATE TABLE [chrFactions](
	[factionID] int NOT NULL,
	[factionName] varchar(100) NULL,
	[description] memo NULL,
	[raceIDs] int NULL,
	[solarSystemID] int NULL,
	[corporationID] int NULL,
	[sizeFactor] double NULL,
	[stationCount] smallint NULL,
	[stationSystemCount] smallint NULL,
	[militiaCorporationID] int NULL,
	[iconID] int NULL,
  CONSTRAINT [chrFactions_PK] PRIMARY KEY ([factionID])
);