CREATE TABLE [crtRecommendations](
	[recommendationID] int NOT NULL,
	[shipTypeID] int NULL,
	[certificateID] int NULL,
	[recommendationLevel] tinyint NOT NULL,
 CONSTRAINT [crtRecommendations_PK] PRIMARY KEY ([recommendationID])
);

CREATE INDEX [crtRecommendations_IX_certificate] ON [crtRecommendations] ([certificateID] ASC);

CREATE INDEX [crtRecommendations_IX_shipType] ON [crtRecommendations] ([shipTypeID] ASC);