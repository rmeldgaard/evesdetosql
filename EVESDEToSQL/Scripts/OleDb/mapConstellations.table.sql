﻿CREATE TABLE [mapConstellations](
	[regionID] int NULL,
	[constellationID] int NOT NULL,
	[constellationName] varchar(100) NULL,
	[x] double NULL,
	[y] double NULL,
	[z] double NULL,
	[xMin] double NULL,
	[xMax] double NULL,
	[yMin] double NULL,
	[yMax] double NULL,
	[zMin] double NULL,
	[zMax] double NULL,
	[factionID] int NULL,
	[radius] double NULL,
 CONSTRAINT [_PK] PRIMARY KEY ([constellationID])
);

CREATE INDEX [mapConstellations_IX_region] ON [mapConstellations] ([regionID] ASC);