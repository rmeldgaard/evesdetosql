﻿CREATE TABLE [invTypeMaterials](
	[typeID] int NOT NULL,
	[materialTypeID] int NOT NULL,
	[quantity] int NOT NULL DEFAULT 0,
 CONSTRAINT [invTypeMaterials_PK] PRIMARY KEY ([typeID], [materialTypeID])
);