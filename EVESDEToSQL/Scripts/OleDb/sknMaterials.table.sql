﻿CREATE TABLE [sknMaterials](
	[skinMaterialID] int NOT NULL,
	[materialSetID] int NOT NULL DEFAULT 0,
	[displayNameID] int NOT NULL DEFAULT 0,
	[material] varchar(255) NOT NULL DEFAULT '',
	[colorHull] varchar(6) NULL,
	[colorWindow] varchar(6) NULL,
	[colorPrimary] varchar(6) NULL,
	[colorSecondary] varchar(6) NULL,
 CONSTRAINT [sknMaterials_PK] PRIMARY KEY ([skinMaterialID])
);