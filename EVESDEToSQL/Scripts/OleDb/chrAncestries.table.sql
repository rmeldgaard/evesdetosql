﻿CREATE TABLE [chrAncestries](
	[ancestryID] tinyint NOT NULL,
	[ancestryName] varchar(100) NULL,
	[bloodlineID] tinyint NULL,
	[description] memo NULL,
	[perception] tinyint NULL,
	[willpower] tinyint NULL,
	[charisma] tinyint NULL,
	[memory] tinyint NULL,
	[intelligence] tinyint NULL,
	[iconID] int NULL,
	[shortDescription] memo NULL,
 CONSTRAINT [chrAncestries_PK] PRIMARY KEY ([ancestryID])
);