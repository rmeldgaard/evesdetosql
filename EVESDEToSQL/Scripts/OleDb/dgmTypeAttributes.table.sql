﻿CREATE TABLE [dgmTypeAttributes](
	[typeID] int NOT NULL,
	[attributeID] smallint NOT NULL,
	[valueInt] int NULL,
	[valueFloat] double NULL,
 CONSTRAINT [dgmTypeAttributes_PK] PRIMARY KEY ([typeID], [attributeID])
);