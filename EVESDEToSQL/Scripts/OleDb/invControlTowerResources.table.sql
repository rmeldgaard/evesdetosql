﻿CREATE TABLE [invControlTowerResources](
	[controlTowerTypeID] int NOT NULL,
	[resourceTypeID] int NOT NULL,
	[purpose] tinyint NULL,
	[quantity] int NULL,
	[minSecurityLevel] double NULL,
	[factionID] int NULL,
 CONSTRAINT [invControlTowerResources_PK] PRIMARY KEY ([controlTowerTypeID], [resourceTypeID])
);