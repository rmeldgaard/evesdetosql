﻿CREATE TABLE [translationTables](
	[sourceTable] varchar(200) NOT NULL,
	[destinationTable] varchar(200) NULL,
	[translatedKey] varchar(200) NOT NULL,
	[tcGroupID] int NULL,
	[tcID] int NULL,
 CONSTRAINT [translationTables_PK] PRIMARY KEY ([sourceTable], [translatedKey])
);