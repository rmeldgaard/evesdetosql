﻿CREATE TABLE [mapDenormalize](
	[itemID] int NOT NULL,
	[typeID] int NULL,
	[groupID] int NULL,
	[solarSystemID] int NULL,
	[constellationID] int NULL,
	[regionID] int NULL,
	[orbitID] int NULL,
	[x] double NULL,
	[y] double NULL,
	[z] double NULL,
	[radius] double NULL,
	[itemName] varchar(100) NULL,
	[security] double NULL,
	[celestialIndex] tinyint NULL,
	[orbitIndex] tinyint NULL,
 CONSTRAINT [_PK] PRIMARY KEY ([itemID])
);

CREATE INDEX [mapDenormalize_IX_constellation] ON [mapDenormalize] ([constellationID] ASC);

CREATE INDEX [mapDenormalize_IX_groupConstellation] ON [mapDenormalize] ([groupID] ASC, [constellationID] ASC);

CREATE INDEX [mapDenormalize_IX_groupRegion] ON [mapDenormalize] ([groupID] ASC, [regionID] ASC);

CREATE INDEX [mapDenormalize_IX_groupSystem] ON [mapDenormalize] ([groupID] ASC, [solarSystemID] ASC);

CREATE INDEX [mapDenormalize_IX_orbit] ON [mapDenormalize] ([orbitID] ASC);

CREATE INDEX [mapDenormalize_IX_region] ON [mapDenormalize] ([regionID] ASC);

CREATE INDEX [mapDenormalize_IX_system] ON [mapDenormalize] ([solarSystemID] ASC);