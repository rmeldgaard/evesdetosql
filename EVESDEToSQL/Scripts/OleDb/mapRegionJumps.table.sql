﻿CREATE TABLE [mapRegionJumps](
	[fromRegionID] int NOT NULL,
	[toRegionID] int NOT NULL,
 CONSTRAINT [mapRegionJumps_PK] PRIMARY KEY ([fromRegionID], [toRegionID])
);