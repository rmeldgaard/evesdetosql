﻿CREATE TABLE [invPositions](
	[itemID] long NOT NULL,
	[x] double NOT NULL DEFAULT 0.0,
	[y] double NOT NULL DEFAULT 0.0,
	[z] double NOT NULL DEFAULT 0.0,
	[yaw] double NULL,
	[pitch] double NULL,
	[roll] double NULL,
 CONSTRAINT [invPositions_PK] PRIMARY KEY ([itemID])
);