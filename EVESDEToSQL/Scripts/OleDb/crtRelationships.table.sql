CREATE TABLE [crtRelationships](
	[relationshipID] int NOT NULL,
	[parentID] int NULL,
	[parentTypeID] int NULL,
	[parentLevel] tinyint NULL,
	[childID] int NULL,
	[grade] tinyint NULL,
 CONSTRAINT [crtRelationships_PK] PRIMARY KEY ([relationshipID])
);

CREATE INDEX [crtRelationships_IX_child] ON [crtRelationships] ([childID] ASC);

CREATE INDEX [crtRelationships_IX_parent] ON [crtRelationships] ([parentID] ASC);