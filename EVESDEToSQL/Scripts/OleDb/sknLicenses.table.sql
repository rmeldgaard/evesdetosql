﻿CREATE TABLE [sknLicenses](
	[licenseTypeID] int NOT NULL,
	[skinID] int NOT NULL,
	[duration] int NOT NULL DEFAULT -1,
 CONSTRAINT [sknLicenses_PK] PRIMARY KEY ([licenseTypeID])
);

CREATE INDEX [sknLicenses_IX_skin] ON [sknLicenses] ([skinID] ASC);