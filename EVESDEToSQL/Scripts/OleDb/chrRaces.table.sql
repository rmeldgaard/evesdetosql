﻿CREATE TABLE [chrRaces](
	[raceID] tinyint NOT NULL,
	[raceName] varchar(100) NULL,
	[description] memo NULL,
	[iconID] int NULL,
	[shortDescription] memo NULL,
 CONSTRAINT [chrRaces_PK] PRIMARY KEY ([raceID])
);