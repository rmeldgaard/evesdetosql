﻿CREATE TABLE [mapConstellationJumps](
	[fromRegionID] int NULL,
	[fromConstellationID] int NOT NULL,
	[toConstellationID] int NOT NULL,
	[toRegionID] int NULL,
 CONSTRAINT [mapConstellationJumps_PK] PRIMARY KEY ([fromConstellationID], [toConstellationID])
);

CREATE INDEX [mapConstellationJumps_IX_fromRegion] ON [mapConstellationJumps] ([fromRegionID] ASC);