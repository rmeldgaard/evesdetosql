﻿CREATE TABLE [staStations](
	[stationID] int NOT NULL,
	[security] smallint NULL,
	[dockingCostPerVolume] double NULL,
	[maxShipVolumeDockable] double NULL,
	[officeRentalCost] int NULL,
	[operationID] tinyint NULL,
	[stationTypeID] int NULL,
	[corporationID] int NULL,
	[solarSystemID] int NULL,
	[constellationID] int NULL,
	[regionID] int NULL,
	[stationName] varchar(100) NULL,
	[x] double NULL,
	[y] double NULL,
	[z] double NULL,
	[reprocessingEfficiency] double NULL,
	[reprocessingStationsTake] double NULL,
	[reprocessingHangarFlag] tinyint NULL,
 CONSTRAINT [staStations_PK] PRIMARY KEY ([stationID])
);

CREATE INDEX [staStations_IX_constellation] ON [staStations] ([corporationID] ASC);

CREATE INDEX [staStations_IX_corporation] ON [staStations] ([corporationID] ASC);

CREATE INDEX [staStations_IX_operation] ON [staStations] ([operationID] ASC);

CREATE INDEX [staStations_IX_region] ON [staStations] ([regionID] ASC);

CREATE INDEX [staStations_IX_system] ON [staStations] ([solarSystemID] ASC);

CREATE INDEX [staStations_IX_type] ON [staStations] ([stationTypeID] ASC);