﻿CREATE TABLE [ramTypeRequirements](
	[typeID] int NOT NULL,
	[activityID] tinyint NOT NULL,
	[requiredTypeID] int NOT NULL,
	[quantity] int NULL,
	[level] int NULL,
	[damagePerJob] double NULL,
	[recycle] bit NULL,
	[raceID] int NULL,
	[probability] double NULL,
	[consume] bit NULL,
 CONSTRAINT [ramTypeRequirements_PK] PRIMARY KEY ([typeID], [activityID], [requiredTypeID])
);