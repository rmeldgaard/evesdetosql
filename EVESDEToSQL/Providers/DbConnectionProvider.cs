﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using EVESDEToSQL.Utils;

namespace EVESDEToSQL.Providers
{
    internal abstract class DbConnectionProvider
    {
        protected string Text;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnectionProvider"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="nameOrConnectionString">The name or connection string.</param>
        protected DbConnectionProvider(Type type, string nameOrConnectionString)
        {
            CreateConnection(type, nameOrConnectionString);
        }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        protected internal DbConnection Connection { get; private set; }

        /// <summary>
        /// Creates the connection.
        /// </summary>
        /// <param name="connectionType">Type of the connection.</param>
        /// <param name="nameOrConnectionString">The name or connection string.</param>
        /// <exception cref="System.ArgumentNullException">nameOrConnectionString</exception>
        private void CreateConnection(Type connectionType, String nameOrConnectionString)
        {
            if (String.IsNullOrWhiteSpace(nameOrConnectionString))
                throw new ArgumentNullException("nameOrConnectionString");

            if (nameOrConnectionString.StartsWith("name=", StringComparison.OrdinalIgnoreCase))
                nameOrConnectionString = nameOrConnectionString.Replace("name=", String.Empty);

            ConnectionStringSettings connectionStringSetting = ConfigurationManager.ConnectionStrings[nameOrConnectionString];

            string connectionString = connectionStringSetting == null
                ? nameOrConnectionString
                : connectionStringSetting.ConnectionString;

            if (connectionType != typeof(SqlConnection))
            {
                GroupCollection groups = Regex.Match(connectionString, "data source=(?<filePath>.*\\.[a-z]+)",
                    RegexOptions.IgnoreCase | RegexOptions.Compiled).Groups;

                if (groups.Count != 2 || !File.Exists(groups["filePath"].Value))
                {
                    if (Console.CursorLeft > 0)
                        Util.ResetConsoleCursorPosition(Text);

                    Console.WriteLine(@"Database {0}file does not exists!",
                        groups.Count != 2
                            ? String.Empty
                            : String.Format(CultureInfo.InvariantCulture,
                                "{0} ", Path.GetFileName(groups["filePath"].Value)));

                    Console.WriteLine();
                    Console.WriteLine();

                    return;
                }
            }

            if (String.IsNullOrWhiteSpace(connectionString))
            {
                Util.ResetConsoleCursorPosition(Text);
                Console.WriteLine(@"Can not find connection string: {0}", nameOrConnectionString);
                Util.PressAnyKey(-1);
            }

            ConstructorInfo ci = connectionType.GetConstructor(new[]
            {
                typeof(string)
            });

            if (ci == null)
                return;

            Connection = (DbConnection)ci.Invoke(new object[]
            {
                connectionString
            });
        }

        /// <summary>
        /// Opens the connection.
        /// </summary>
        protected internal void OpenConnection()
        {
            if (Connection == null)
                return;

            string databaseTypeName = Connection is SqlConnection
                ? "SQL Server"
                : Connection.GetType().Name.Replace("Connection", String.Empty);

            string databaseName = String.IsNullOrWhiteSpace(Connection.Database) ? "default" : Connection.Database;

            Text = String.Format(CultureInfo.InvariantCulture, "Connecting to {0} '{1}' database... ",
                databaseTypeName, databaseName);

            Console.Write(Text);

            try
            {
                Connection.Open();

                Util.ResetConsoleCursorPosition(Text);
                Console.WriteLine(@"Connection to {0} '{1}' database: Successful", databaseTypeName, databaseName);
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string text = String.Format(CultureInfo.InvariantCulture, "Connection to {0} '{1}' database: Failed",
                    databaseTypeName, databaseName);
                Util.HandleExceptionWithReason(Text, text, ex.Message);
            }
        }

        /// <summary>
        /// Closes the connection.
        /// </summary>
        protected internal void CloseConnection()
        {
            if (Connection == null || Connection.State == ConnectionState.Closed)
                return;

            string databaseTypeName = Connection is SqlConnection
                ? "SQL Server"
                : Connection.GetType().Name.Replace("Connection", String.Empty);

            string databaseName = String.IsNullOrWhiteSpace(Connection.Database) ? "default" : Connection.Database;

            Text = String.Format(CultureInfo.InvariantCulture, "Disconnecting from {0} '{1}' database... ",
                databaseTypeName, databaseName);
            Console.Write(Text);

            try
            {
                Connection.Close();

                Util.ResetConsoleCursorPosition(Text);
                Console.WriteLine(@"Disconnection from {0} '{1}' database: Successful",
                    databaseTypeName, databaseName);
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string text = String.Format(CultureInfo.InvariantCulture, "Disconnection from {0} '{1}' database: Failed",
                    databaseTypeName, databaseName);
                Util.HandleExceptionWithReason(Text, text, ex.Message);
            }
            finally
            {
                Connection = null;
            }
        }

        /// <summary>
        /// SQL insert command text.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected internal static string InsertCommandText(String tableName, IDictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                sb.Append(parameter.Key);
                if (!parameter.Equals(parameters.Last()))
                    sb.Append(", ");
            }
            sb.Append(") VALUES (");

            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                sb.Append(parameter.Value);
                sb.Append(!parameter.Equals(parameters.Last()) ? ", " : ")");
            }

            return String.Format(CultureInfo.InvariantCulture, "INSERT INTO {0} {1}", tableName, sb);
        }

        /// <summary>
        /// SQL update command text.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected internal static string UpdateCommandText(String tableName, IDictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();
            Dictionary<string, string> pars = parameters
                .Where(par =>
                    !par.Key.StartsWith("columnFilter", StringComparison.OrdinalIgnoreCase) &&
                    !par.Key.StartsWith("id", StringComparison.OrdinalIgnoreCase))
                .ToDictionary(pair => pair.Key, pair => pair.Value);

            foreach (var parameter in pars)
            {
                sb.AppendFormat("{0} = {1}", parameter.Key, parameter.Value);
                if (!parameter.Equals(pars.Last()))
                    sb.Append(", ");
            }

            if (!String.IsNullOrWhiteSpace(parameters["columnFilter1"]) && !String.IsNullOrWhiteSpace(parameters["id1"]))
            {
                for (int i = 1; i < 5; i++)
                {
                    string filterName = "columnFilter" + i;
                    string idName = "id" + i;

                    if (parameters.ContainsKey(filterName) &&
                        parameters.ContainsKey(idName) &&
                        !String.IsNullOrWhiteSpace(parameters[filterName]) &&
                        !String.IsNullOrWhiteSpace(parameters[idName]))
                    {
                        sb.AppendFormat("{0}{1} = {2}", i == 1 ? " WHERE " : " AND ", parameters[filterName], parameters[idName]);
                    }
                }
            }

            return String.Format(CultureInfo.InvariantCulture, "UPDATE {0} SET {1}", tableName, sb);
        }

        /// <summary>
        /// SQL delete command text.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected internal static string DeleteCommandText(String tableName, IDictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();

            if (!String.IsNullOrWhiteSpace(parameters["columnFilter1"]) && !String.IsNullOrWhiteSpace(parameters["id1"]))
            {
                for (int i = 1; i < 5; i++)
                {
                    string filterName = "columnFilter" + i;
                    string idName = "id" + i;

                    if (parameters.ContainsKey(filterName) &&
                        parameters.ContainsKey(idName) &&
                        !String.IsNullOrWhiteSpace(parameters[filterName]) &&
                        !String.IsNullOrWhiteSpace(parameters[idName]))
                    {
                        sb.AppendFormat("{0}{1} = {2}", i == 1 ? " WHERE " : " AND ", parameters[filterName], parameters[idName]);
                    }
                }
            }

            return String.Format(CultureInfo.InvariantCulture, "DELETE {0}{1}", tableName, sb);
        }

        /// <summary>
        /// Shrinks the database.
        /// </summary>
        protected internal virtual void ShrinkDatabase()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Reorganizes all indexes.
        /// </summary>
        protected internal virtual void ReorganizeAllIndexes()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Drops and Creates the specified table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        protected internal abstract void DropAndCreateTable(String tableName);

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="notify">if set to <c>true</c> notifies on row change.</param>
        protected internal abstract void ImportDataBulk(DataTable dataTable, bool notify = true);
    }
}
