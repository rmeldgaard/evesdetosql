﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Globalization;
using EVESDEToSQL.Utils;

namespace EVESDEToSQL.Providers
{
    internal sealed class OleDbConnectionProvider : DbConnectionProvider
    {
        private readonly OleDbConnection m_oleDbConnection;

        private bool m_isClosing;
        private double m_rowCount;

        public OleDbConnectionProvider(string nameOrConnectionString)
            : base(typeof(OleDbConnection), nameOrConnectionString)
        {
            m_oleDbConnection = (OleDbConnection)Connection;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Drops and Creates the specified table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        protected internal override void DropAndCreateTable(String tableName)
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            using (DbCommand command = new OleDbCommand(
                String.Empty,
                m_oleDbConnection,
                m_oleDbConnection.BeginTransaction()))
            {
                try
                {
                    foreach (string commandText in Util.GetScriptFor<OleDbCommand>(tableName)
                        .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        command.CommandText = commandText;
                        command.ExecuteNonQuery();
                    }

                    command.Transaction.Commit();
                }
                catch (OleDbException ex)
                {
                    Util.HandleExceptionForCommand(command, ex);

                    if (command.Transaction != null)
                        command.Transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="notify">if set to <c>true</c> notifies on row change.</param>
        protected internal override void ImportDataBulk(DataTable dataTable, bool notify = true)
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            m_rowCount = Math.Round((double)dataTable.Rows.Count * 2, 0, MidpointRounding.AwayFromZero);

            using (OleDbBulkCopy oleDbBulkCopy = new OleDbBulkCopy(Connection.ConnectionString))
            {
                oleDbBulkCopy.DestinationTableName = dataTable.TableName;
                oleDbBulkCopy.NotifyAfter = 1;

                if (notify)
                    oleDbBulkCopy.OleDbRowsCopied += OleDbBulkCopy_OleDbRowsCopied;

                try
                {
                    oleDbBulkCopy.WriteToFile(dataTable);
                }
                catch (Exception e)
                {
                    string text = String.Format(CultureInfo.InvariantCulture,
                        "Unable to import {0}                ", dataTable.TableName);
                    Util.HandleExceptionWithReason(Text, text, e.Message);
                }
                finally
                {
                    if (notify)
                        oleDbBulkCopy.OleDbRowsCopied -= OleDbBulkCopy_OleDbRowsCopied;
                }
            }
        }

        /// <summary>
        /// Handles the OleDbRowsCopied event of the OleDbBulkCopy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="OleDbRowsCopiedEventArgs" /> instance containing the event data.</param>
        private void OleDbBulkCopy_OleDbRowsCopied(object sender, OleDbRowsCopiedEventArgs e)
        {
            Util.UpdatePercentDone(m_rowCount);
        }

        private sealed class OleDbRowsCopiedEventArgs : EventArgs
        {
            public new static readonly OleDbRowsCopiedEventArgs Empty = new OleDbRowsCopiedEventArgs();
        }

        private sealed class OleDbBulkCopy : IDisposable
        {
            internal event EventHandler<OleDbRowsCopiedEventArgs> OleDbRowsCopied;

            private string m_destinationTableName;
            private OleDbConnection m_connection;
            private OleDbTransaction m_internalTransaction;

            /// <summary>
            /// Initializes a new instance of the <see cref="OleDbBulkCopy"/> class.
            /// </summary>
            /// <param name="connectionString">The connection string.</param>
            /// <exception cref="System.ArgumentNullException">connectionString</exception>
            internal OleDbBulkCopy(string connectionString)
            {
                if (String.IsNullOrWhiteSpace(connectionString))
                    throw new ArgumentNullException("connectionString");

                m_connection = new OleDbConnection(connectionString);

                if (m_connection.State == ConnectionState.Closed)
                    m_connection.Open();

                m_internalTransaction = m_connection.BeginTransaction();
            }

            /// <summary>
            /// Gets or sets the name of the destination table.
            /// </summary>
            /// <value>
            /// The name of the destination table.
            /// </value>
            /// <exception cref="System.ArgumentNullException">DestinationTableName</exception>
            /// <exception cref="System.ArgumentOutOfRangeException">DestinationTableName</exception>
            internal string DestinationTableName
            {
                get { return m_destinationTableName; }
                set
                {
                    if (value == null)
                        throw new ArgumentNullException("DestinationTableName");

                    if (value.Length == 0)
                        throw new ArgumentOutOfRangeException("DestinationTableName");

                    m_destinationTableName = value;
                }
            }

            /// <summary>
            /// Gets or sets the notify after.
            /// </summary>
            /// <value>
            /// The notify after.
            /// </value>
            internal int NotifyAfter { get; set; }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            void IDisposable.Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Releases unmanaged and - optionally - managed resources.
            /// </summary>
            /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
            private void Dispose(bool disposing)
            {
                if (!disposing)
                    return;

                try
                {
                    if (m_internalTransaction == null)
                        return;

                    m_internalTransaction.Dispose();
                    m_internalTransaction = null;
                }
                catch (Exception ex)
                {
                    Util.HandleException(String.Empty, ex.Message);
                }
                finally
                {
                    if (m_connection != null)
                    {
                        m_connection.Close();
                        m_connection = null;
                    }
                }
            }

            /// <summary>
            /// Writes to server.
            /// </summary>
            /// <param name="table">The table.</param>
            /// <exception cref="System.ArgumentNullException">table</exception>
            internal void WriteToFile(DataTable table)
            {
                if (table == null)
                    throw new ArgumentNullException("table");

                string commandText = String.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}]", table.TableName);

                try
                {
                    using (OleDbCommand command = new OleDbCommand(commandText, m_connection, m_internalTransaction))
                    using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                    using (OleDbCommandBuilder builder = new OleDbCommandBuilder(adapter))
                    {
                        builder.QuotePrefix = "[";
                        builder.QuoteSuffix = "]";

                        adapter.RowUpdated += Adapter_RowUpdated;
                        adapter.Update(table);
                        adapter.RowUpdated -= Adapter_RowUpdated;

                        m_internalTransaction.Commit();
                    }
                }
                catch (OleDbException ex)
                {
                    Util.HandleException(String.Empty, ex.Message);
                    m_internalTransaction.Rollback();
                }
            }

            /// <summary>
            /// Handles the RowUpdated event of the Adapter control.
            /// </summary>
            /// <param name="sender">The source of the event.</param>
            /// <param name="e">The <see cref="RowUpdatedEventArgs"/> instance containing the event data.</param>
            private void Adapter_RowUpdated(object sender, RowUpdatedEventArgs e)
            {
                if (e.RowCount != NotifyAfter)
                    return;

                if (OleDbRowsCopied != null)
                    OleDbRowsCopied(null, OleDbRowsCopiedEventArgs.Empty);
            }
        }
    }
}
