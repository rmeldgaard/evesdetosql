﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Providers
{
    internal sealed class SqlConnectionProvider : DbConnectionProvider
    {
        private readonly SqlConnection m_sqlConnection;

        private bool m_isClosing;
        private double m_rowCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlConnectionProvider"/> class.
        /// </summary>
        /// <param name="nameOrConnectionString">The name or connection string.</param>
        internal SqlConnectionProvider(String nameOrConnectionString)
            : base(typeof(SqlConnection), nameOrConnectionString)
        {
            m_sqlConnection = (SqlConnection)Connection;

            Server = GetServerInstance();

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets the server.
        /// </summary>
        /// <value>
        /// The server.
        /// </value>
        internal Server Server { get; private set; }

        /// <summary>
        /// Gets the server instance.
        /// </summary>
        private Server GetServerInstance()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(m_sqlConnection.ConnectionString);

            SqlConnectionInfo sci = new SqlConnectionInfo
            {
                ServerName = builder.DataSource,
                DatabaseName = "master",
                UseIntegratedSecurity = builder.IntegratedSecurity,
                EncryptConnection = builder.Encrypt,
            };

            if (!builder.IntegratedSecurity)
            {
                sci.UserName = builder.UserID;
                sci.Password = builder.Password;
            }

            ServerConnection serverConnection = new ServerConnection(sci)
            {
                MultipleActiveResultSets = builder.MultipleActiveResultSets,
            };

            return new Server(serverConnection);
        }

        /// <summary>
        /// Gets the database.
        /// </summary>
        internal Database GetDatabase()
        {
            if (Connection == null)
                throw new NoNullAllowedException("Connection was closed.");

            Database database = Server.Databases[Connection.Database];

            if (database != null)
                return database;

            string failText = String.Format(CultureInfo.InvariantCulture,
                "Database '{0}' not found.",
                Connection.Database);

            throw new NullReferenceException(failText);
        }

        /// <summary>
        /// Shrinks the database.
        /// </summary>
        protected internal override void ShrinkDatabase()
        {
            string databaseName = Connection != null
                ? String.Format(" '{0}'", Connection.Database)
                : String.Empty;
            string actionText = String.Format("Shrinking database{0}...", databaseName);

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            Console.Write(actionText);

            try
            {
                GetDatabase().Shrink(0, ShrinkMethod.Default);
            }
            catch (Exception ex)
            {
                string failText = String.Format(CultureInfo.InvariantCulture,
                    "Shrinking database{0}: Failed\n{1}",
                    databaseName,
                    ex.Message);

                Util.HandleException(actionText, failText);
            }

            Util.DisplayEndTime(stopwatch);

            Console.WriteLine();
        }

        /// <summary>
        /// Reorganizes all indexes.
        /// </summary>
        protected internal override void ReorganizeAllIndexes()
        {
            string databaseName = Connection != null
                ? String.Format(" '{0}'", Connection.Database)
                : String.Empty;
            string actionText = String.Format("Reorganizing indexes of database{0}... ", databaseName);

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            Console.Write(actionText);

            try
            {
                List<Index> indexes = GetDatabase().Tables.Cast<Table>().SelectMany(table => table.Indexes.Cast<Index>()).ToList();

                foreach (Index index in indexes)
                {
                    Util.UpdatePercentDone(indexes.Count);

                    index.ReorganizeAllIndexes();
                }
            }
            catch (Exception ex)
            {
                string failText = String.Format(CultureInfo.InvariantCulture,
                    "Reorganizing indexes of database{0}: Failed\n{1}",
                    databaseName,
                    ex.Message);

                Util.HandleException(actionText, failText);
            }

            Util.DisplayEndTime(stopwatch);

            Console.WriteLine();
        }

        /// <summary>
        /// Creates the table.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="searchKey">The search key.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columns">The columns.</param>
        /// <returns>
        ///   <c>true</c> if the table is empty; otherwise, <c>false</c>.
        /// </returns>
        internal bool CreateTableOrColumns(YamlMappingNode rNode, string searchKey, string tableName,
            IDictionary<string, string> columns)
        {
            if (DropAndCreateTable(rNode, searchKey, tableName))
                return true;

            CreateColumns(tableName, columns);
            return false;
        }

        /// <summary>
        /// Drops and Creates the specified table.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="searchKey">The search key.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        internal bool DropAndCreateTable(YamlMappingNode rNode, string searchKey, string tableName)
        {
            if (!rNode.Children.Select(pair => pair.Value)
                .OfType<YamlMappingNode>()
                .Select(cNode => cNode.Any(x => x.Key.ToString() == searchKey))
                .Any(createTable => createTable))
            {
                return false;
            }

            DropAndCreateTable(tableName);
            return true;
        }

        /// <summary>
        /// Drops and Creates the specified table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        protected internal override void DropAndCreateTable(String tableName)
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            using (DbCommand command = new SqlCommand(
                Util.GetScriptFor<SqlCommand>(tableName),
                m_sqlConnection,
                m_sqlConnection.BeginTransaction()))
            {
                try
                {
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                }
                catch (SqlException e)
                {
                    Util.HandleExceptionForCommand(command, e);

                    if (command.Transaction != null)
                        command.Transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Creates the columns.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columns">The columns.</param>
        private void CreateColumns(String tableName, IDictionary<string, string> columns)
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            if (
                Connection.GetSchema("columns")
                    .Select(String.Format(CultureInfo.InvariantCulture, "TABLE_NAME = '{0}'", tableName))
                    .Length == 0)
            {
                Console.WriteLine(@"Can't find table '{0}'.", tableName);
                Util.PressAnyKey(-1);
            }

            foreach (KeyValuePair<string, string> column in columns)
            {
                CreateColumn(tableName, column.Key, column.Value);
            }
        }

        /// <summary>
        /// Creates the column.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="columnType">Type of the column.</param>
        /// <param name="defaultValue">The default value.</param>
        private void CreateColumn(String tableName, String columnName, String columnType, string defaultValue = "Null")
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            if (Connection.GetSchema("columns")
                .Select(String.Format(CultureInfo.InvariantCulture, "TABLE_NAME = '{0}' AND COLUMN_NAME = '{1}'", tableName,
                    columnName))
                .Length != 0)
            {
                return;
            }

            double number;
            string commandText = String.Format(CultureInfo.InvariantCulture, "ALTER TABLE {0} ADD {1} {2} {3} NULL {4}", tableName,
                columnName, columnType,
                defaultValue != SqlString.Null.ToString() ? "NOT" : String.Empty,
                defaultValue != SqlString.Null.ToString()
                    ? String.Format(CultureInfo.InvariantCulture, "DEFAULT ({0})",
                        Double.TryParse(defaultValue, out number)
                            ? String.Format(CultureInfo.InvariantCulture, "({0})", defaultValue)
                            : String.Format(CultureInfo.InvariantCulture, "'{0}'", defaultValue)
                        )
                    : String.Empty);

            using (DbCommand command = new SqlCommand(
                commandText,
                m_sqlConnection,
                m_sqlConnection.BeginTransaction()))
            {
                try
                {
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                }
                catch (SqlException e)
                {
                    Util.HandleExceptionForCommand(command, e);

                    if (command.Transaction != null)
                        command.Transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="notify">if set to <c>true</c> notifies on row change.</param>
        protected internal override void ImportDataBulk(DataTable dataTable, bool notify = true)
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            m_rowCount = Math.Round((double)dataTable.Rows.Count * 2, 0, MidpointRounding.AwayFromZero);

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                (SqlConnection)Connection,
                SqlBulkCopyOptions.UseInternalTransaction,
                null))
            {
                // Set the timeout of the operation accordingly to the setting in the connection string
                // if it's greater than the default
                if (Connection.ConnectionTimeout > sqlBulkCopy.BulkCopyTimeout)
                    sqlBulkCopy.BulkCopyTimeout = Connection.ConnectionTimeout;

                sqlBulkCopy.DestinationTableName = dataTable.TableName;
                sqlBulkCopy.NotifyAfter = 1;

                if (notify)
                    sqlBulkCopy.SqlRowsCopied += SqlBulkCopy_SqlRowsCopied;

                try
                {
                    sqlBulkCopy.WriteToServer(dataTable);
                }
                catch (Exception ex)
                {
                    string text = String.Format(CultureInfo.InvariantCulture,
                        "Unable to import {0}                ", dataTable.TableName);
                    Util.HandleExceptionWithReason(Text, text, ex.Message);
                }
                finally
                {
                    if (notify)
                        sqlBulkCopy.SqlRowsCopied -= SqlBulkCopy_SqlRowsCopied;
                }
            }
        }

        /// <summary>
        /// Handles the SqlRowsCopied event of the SqlBulkCopy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SqlRowsCopiedEventArgs"/> instance containing the event data.</param>
        private void SqlBulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Util.UpdatePercentDone(m_rowCount);
        }

        /// <summary>
        /// Gets the data table.
        /// </summary>
        /// <param name="table">The table.</param>
        /// <returns></returns>
        internal DataTable GetDataTable(Table table)
        {
            Util.UpdatePercentDone(0);

            string commandText = String.Format(CultureInfo.InvariantCulture, "SELECT * FROM [{0}]", table.Name);

            m_rowCount = Math.Round((double)table.RowCount * 2, 0, MidpointRounding.AwayFromZero);

            // Usage of SqlDataAdapter to get the table as DataTable
            using (DataTable dataTable = new DataTable(table.Name))
            using (SqlCommand command = new SqlCommand(commandText, m_sqlConnection))
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command))
            {
                dataTable.RowChanged += DataTable_RowChanged;

                // Fill the DataTable with the result of the SQL statement
                sqlDataAdapter.AcceptChangesDuringFill = false;
                sqlDataAdapter.FillSchema(dataTable, SchemaType.Source);
                sqlDataAdapter.Fill(dataTable);

                dataTable.RowChanged -= DataTable_RowChanged;

                return dataTable;
            }
        }

        /// <summary>
        /// Handles the RowChanged event of the DataTable control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataRowChangeEventArgs"/> instance containing the event data.</param>
        private void DataTable_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            Util.UpdatePercentDone(m_rowCount);
        }
    }
}
